#!/usr/bin/env python

#
# python sample application that reads from the raspicomm's RS-485 Port
#

import array
import serial


readBuffer = []

print('this sample application reads from the rs-485 port')

# open the port
print('opening device /dev/ttyRPC0')
try:
    ser = serial.Serial(port='/dev/ttyRPC0', baudrate=9600)
except:
    print('failed.')
    print('possible causes:')
    print('1) the raspicomm device driver is not loaded. type \'lsmod\' and verify that you \'raspicommrs485\' is loaded.')
    print('2) the raspicomm device driver is in use. Is another application using the device driver?')
    print('3) something went wrong when loading the device driver. type \'dmesg\' and check the kernel messages')
    exit()
print('successful.')

# read in a loop

print('start reading from the rs-485 port ')
Hold=1
while Hold==1:
    readCount=0
    while readCount < ser.inWaiting():
        readBuffer.append(ser.read(1).decode('utf-8'))
        readCount=readCount+1
        Hold=0

# print the received bytes
print('we received the following bytes:')
i=0
while i < len(readBuffer):
    
    hx=''
    hx=' - \'{0}\''.format(readBuffer[i])
    print('[{0:d}]:{2}'.format(i, hx))    
    i=i+1