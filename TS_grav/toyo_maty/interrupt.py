#!/usr/bin/env python2.7  
# script by Alex Eames http://RasPi.tv/  
# http://raspi.tv/2013/how-to-use-interrupts-with-python-on-the-raspberry-pi-and-rpi-gpio  
import RPi.GPIO as GPIO  
GPIO.setmode(GPIO.BCM)  
  
# GPIO 20 set up as input. It is pulled up to stop false signals  
GPIO.setup(19, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)  
  
def my_callback(channel):  
    print "Rising edge detected on port 21"
print "Make sure you have a button connected so that when pressed"  
print "it will connect GPIO port 21 (pin 38) to GND (pin 6)\n"  
raw_input("Press Enter when ready\n>")  
  
print "Waiting for falling edge on port 21"  
# now the program will do nothing until the signal on port 20   
# starts to fall towards zero. This is why we used the pullup  
# to keep the signal high and prevent a false interrupt  
  
print "During this waiting time, your computer is not"   
print "wasting resources by polling for a button press.\n"  
print "Press your button when ready to initiate a falling edge interrupt."  
  
try:  
#    GPIO.wait_for_edge(21, GPIO.FALLING)  
    GPIO.add_event_detect(19, GPIO.RISING, callback=my_callback, bouncetime=300)
    print "\nFalling edge detected. Now your program can continue with"  
    print "whatever was waiting for a button press."  
except KeyboardInterrupt:  
    GPIO.cleanup()       # clean up GPIO on CTRL+C exit  
GPIO.cleanup()           # clean up GPIO on normal exit  