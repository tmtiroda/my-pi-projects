#!/usr/bin/env python2.7  
# script by Alex Eames http://RasPi.tv  
# http://RasPi.tv/how-to-use-interrupts-with-python-on-the-raspberry-pi-and-rpi-gpio-part-3  
import RPi.GPIO as GPIO  
GPIO.setmode(GPIO.BCM)  

import smbus
import time
import binascii

            
class Outputhandlers1:
    
    def OutputHandler(*args):
        print "here comes Output handlers"
        print "Elsoveg",  inputs1.Elsoveg
        print "Hatsoveg",  inputs1.Hatsoveg
        print "Balgomb",  inputs1.Balgomb
        print "Jobbgomb",  inputs1.Jobbgomb
        print "ManualM",  inputs1.ManualM
        print "Elsoind",  inputs1.ElsoInd
        print "HatsoInd",  inputs1.HatsoInd
        print "Veszkor",  inputs1.Veszkor
        if iputs1.Jobbgomb==1 and inputs1.balgomb==1:
                            
class InnpHandlers2:

    #Input handler functions
    def Inphandler1(self,Value):
        if Value==1:
            print "inp1=1"
        elif Value==0:
            print "inp1=0"
        else:
            print "input read error"
        
    def Inphandler2(self,Value):
        if Value==1:
            print "inp2=1"
        elif Value==0:
            print "inp2=0"
        else:
            print "input read error"
            
    def Inphandler3(self,Value):
        if Value==1:
            print "inp3=1"
        elif Value==0:
            print "inp3=0"
        else:
            print "input read error"
            
    def Inphandler4(self,Value):
        if Value==1:
            print "inp4=1"
        elif Value==0:
            print "inp4=0"
        else:
            print "input read error"
            
    def Inphandler5(self,Value):
        if Value==1:
            print "inp5=1"
        elif Value==0:
            print "inp5=0"
        else:
            print "input read error"
            
    def Inphandler6(self,Value):
        if Value==1:
            print "inp6=1"
        elif Value==0:
            print "inp6=0"
        else:
            print "input read error"
            
    def Inphandler7(self,Value):
        if Value==1:
            print "inp7=1"
        elif Value==0:
            print "inp7=0"
        else:
            print "input read error"
            
    def Inphandler8(self,Value):
        if Value==1:
            print "inp8=1"
        elif Value==0:
            print "inp8=0"
        else:
            print "input read error"
            
class Outputhandlers2:
    
    def OutputHandler(*args):
        print "here comes Output handlers"
    

    
# i2c address of PCF8574
PCF8574=0x39
PCF8573=0x38

# open the bus (0 -- original Pi, 1 -- Rev 2 Pi)
b=smbus.SMBus(1)

class innput1:
    
    Elsoveg=0
    Hatsoveg=0
    Balgomb=0
    Jobbgomb=0
    ManualM=0
    ElsoInd=0
    HatsoInd=0
    Veszkor=0
    
    def __init__(self,RawInp='',PCF8574=0x39,in1=0,in2=0,in3=0,in4=0,in5=0,in6=0,in7=0,in8=0):
        self.InpData=''
        self.Inputs=[self.Elsoveg,self.Hatsoveg,self.Balgomb,self.Jobbgomb,self.ManualM,self.ElsoInd,self.HatsoInd,self.Veszkor]
    
        try:
            self.RawInp =int(b.read_byte(PCF8574))
            self.InpData='{0:08b}'.format(self.RawInp)
            print "initiated"
            
        except:
            print    "Input_Mallfunction"
        
    def GetInp(self):
        print "get Inp function"
        tin1=self.InpData[0]
        tin2=self.InpData[1]
        tin3=self.InpData[2]
        tin4=self.InpData[3]
        tin5=self.InpData[4]
        tin6=self.InpData[5]
        tin7=self.InpData[6]
        tin8=self.InpData[7]
    
        #input value 
        self.in1=int(not(int(tin1)))
        self.in2=int(not(int(tin2)))
        self.in3=int(not(int(tin3)))
        self.in4=int(not(int(tin4)))
        self.in5=int(not(int(tin5)))
        self.in6=int(not(int(tin6)))
        self.in7=int(not(int(tin7)))
        self.in8=int(not(int(tin8)))
        
        #inputs by names
        self.Elsoveg=self.in1
        self.Hatsoveg=self.in2
        self.Balgomb=self.in3
        self.Jobbgomb=self.in4
        self.ManualM=self.in6
        self.ElsoInd=self.in7
        self.HatsoInd=self.in8
        self.Veszkor=self.in5
         
        #input functions
        Outputhandlers1().OutputHandler(self.Elsoveg,self.Hatsoveg,self.Balgomb,self.Jobbgomb,self.ManualM,self.ElsoInd,self.HatsoInd,self.Veszkor)
        
class innput2:
    
    MarkError=0
    MarkReady=0
    MarkStartPos=0
    Jobbgomb2=0
    ManualM2=0
    ElsoInd2=0
    HatsoInd2=0
    Veszkor2=0
    
    def __init__(self,RawInp='',PCF8573=0x38,in1=0,in2=0,in3=0,in4=0,in5=0,in6=0,in7=0,in8=0):
        self.InpData=''
        self.Inputs=[self.MarkError,self.MarkReady,self.MarkStartPos,self.Jobbgomb2,self.ManualM2,self.ElsoInd2,self.HatsoInd2,self.Veszkor2]
    
        try:
            self.RawInp =int(b.read_byte(PCF8573))
            self.InpData='{0:08b}'.format(self.RawInp)
            print "initiated"
            
        except:
            print    "Input_Mallfunction"
        
    def GetInp(self):
        print "get Inp function"
        tin1=self.InpData[0]
        tin2=self.InpData[1]
        tin3=self.InpData[2]
        tin4=self.InpData[3]
        tin5=self.InpData[4]
        tin6=self.InpData[5]
        tin7=self.InpData[6]
        tin8=self.InpData[7]
    
        #input value 
        self.in1=int(not(int(tin1)))
        self.in2=int(not(int(tin2)))
        self.in3=int(not(int(tin3)))
        self.in4=int(not(int(tin4)))
        self.in5=int(not(int(tin5)))
        self.in6=int(not(int(tin6)))
        self.in7=int(not(int(tin7)))
        self.in8=int(not(int(tin8)))
        
        #inputs by names
        self.MarkError=self.in1
        self.MarkReady=self.in2
        self.MarkStartPos=self.in3
        self.Jobbgomb2=self.in4
        self.ManualM2=self.in6
        self.ElsoInd2=self.in7
        self.HatsoInd2=self.in8
        self.Veszkor2=self.in5
         
        #input functions
        InnpHandlers2().Inphandler1(self.in1)
        InnpHandlers2().Inphandler2(self.in2)
        InnpHandlers2().Inphandler3(self.in3)
        InnpHandlers2().Inphandler4(self.in4)
        InnpHandlers2().Inphandler5(self.in5)
        InnpHandlers2().Inphandler6(self.in6)
        InnpHandlers2().Inphandler7(self.in7)
        InnpHandlers2().Inphandler8(self.in8)
        Outputhandlers2().OutputHandler(self.MarkError,self.MarkReady,self.MarkStartPos,self.Jobbgomb2,self.ManualM2,self.ElsoInd2,self.HatsoInd2,self.Veszkor2)     
       
inputs2=innput2()
inputs1=innput1() 
# GPIO 23 & 17 set up as inputs, pulled up to avoid false detection.  
# Both ports are wired to connect to GND on button press.  
# So we'll be setting up falling edge detection for both  
GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_UP)  
  
# GPIO 24 set up as an input, pulled down, connected to 3V3 on button press  
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP)  

"""
GPIO.setup(4, GPIO.OUT, pull_up_down=GPIO.PUD_UP)
GPIO.setup(5, GPIO.OUT, pull_up_down=GPIO.PUD_UP)
GPIO.setup(6, GPIO.OUT, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(12, GPIO.OUT, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(13, GPIO.OUT, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(16, GPIO.OUT, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(17, GPIO.OUT, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(18, GPIO.OUT, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(19, GPIO.OUT, pull_up_down=GPIO.PUD_UP)    
GPIO.setup(22, GPIO.OUT, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(23, GPIO.OUT, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(25, GPIO.OUT, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(26, GPIO.OUT, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(27, GPIO.OUT, pull_up_down=GPIO.PUD_UP)  
"""

# now we'll define two threaded callback functions  

#Event detects


# these will run in another thread when our events are detected  
def my_callback(channel): 
    # i2c address of PCF8574
    inputs1.GetInp()
    print "falling edge detected on 21"  
  
def my_callback2(channel):  
    print "falling edge detected on 23"
    inputs2.GetInp()
  
print "Make sure you have a button connected so that when pressed"  
print "it will connect GPIO port 23 (pin 16) to GND (pin 6)\n"  
print "You will also need a second button connected so that when pressed"  
print "it will connect GPIO port 24 (pin 18) to 3V3 (pin 1)\n"  
print "You will also need a third button connected so that when pressed"  
print "it will connect GPIO port 17 (pin 11) to GND (pin 14)"  
raw_input("Press Enter when ready\n>")  
  
# when a falling edge is detected on port 17, regardless of whatever   
# else is happening in the program, the function my_callback will be run  
GPIO.add_event_detect(21, GPIO.FALLING, callback=my_callback, bouncetime=5)  
  
# when a falling edge is detected on port 23, regardless of whatever   
# else is happening in the program, the function my_callback2 will be run  
# 'bouncetime=300' includes the bounce control written into interrupts2a.py  
GPIO.add_event_detect(20, GPIO.FALLING, callback=my_callback2, bouncetime=5)  
  
try:  
    print "Waiting for rising edge on port 24"  
    GPIO.wait_for_edge(24, GPIO.RISING)  
    print "Rising edge detected on port 24. Here endeth the third lesson."  
  
except KeyboardInterrupt:  
    GPIO.cleanup()       # clean up GPIO on CTRL+C exit  
GPIO.cleanup()           # clean up GPIO on normal exit  