#!/usr/bin/env python

#
# python sample application that reads from the raspicomm's RS-232 Port
#

import string
import serial

class rs2read:
    def rs232read(self):
        'Read RS232 data'
        readBuffer = []
        readCount = 0
        count = 0

        print('this sample application reads from the rs-232 port')

        # open the port
        print('opening device /dev/ttyRPC0')
        try:
            ser = serial.Serial(port='/dev/ttyRPC0', baudrate=9600)
        except:
            print('failed.')
            print('possible causes:')
            print('1) the raspicomm device driver is not loaded. type \'lsmod\' and verify that you \'raspicommrs485\' is loaded.')
            print('2) the raspicomm device driver is in use. Is another application using the device driver?')
            print('3) something went wrong when loading the device driver. type \'dmesg\' and check the kernel messages')
            exit()

        print('successful.')

        # read in a loop
        print('start reading from the rs-232 port')
        val=0
        while val == 0:
            if ser.inWaiting() > 0:
                readCount=ser.inWaiting()
                print ("elso buffer szam :", readCount)
                readBuffer.append(ser.read(readCount).decode('utf-8'))
                readCount2=ser.inWaiting()
                print ("Adat: ", readBuffer)
                print ("ovasas utani buffer szam: ", readCount2)
                if readCount<8:
                    val=1
        ser.close()
                #i=0
                #while i <  readCount:
                #    readBuffer.append(ser.read(1).decode('utf-8'))
                #    i=i+1
                #count = count + readCount
                #if readCount < 8:
                

        # print the received bytes
        print('we received the following bytes:')
        print readBuffer

        #if readCount > 0:
            #kiv='';
            #print (kiv.join(readBuffer))
            #print(count)
            #stringunited=kiv.join(readBuffer)
            #string_modified= stringunited.decode('utf-8');
            #print(stringunited)
            #print(string_modified)
        #    print readBuffer
        #return string_modified
        
    
red=rs2read()
red.rs232read()