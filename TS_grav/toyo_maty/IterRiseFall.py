
#!/usr/bin/env python2.7  
# demo of "BOTH" bi-directional edge detection  
# script by Alex Eames http://RasPi.tv  
# http://raspi.tv/?p=6791  
  
import RPi.GPIO as GPIO  
from time import sleep     # this lets us have a time delay (see line 12)  

import smbus
import time
import binascii
import string
import OPP

Oput=OPP.OPUT()

GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering  
    # set GPIO25 as input (button)
GPIO.setup(19, GPIO.IN, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_UP)  


b=smbus.SMBus(1)
PCF8574=0x39

def Asztal():
    Oput.OPa4=1
    Oput.Write()
    print "asztal"
    
def AsztalM():
    Oput.OPa4=0
    Oput.Write()
    print "Asztalki"
    

    
    
    

def OutputHandler(Elsoveg,Hatsoveg,Balgomb,Jobbgomb,ManualM,ElsoInd,HatsoInd,Veszkor):
    print "outputs"
    Eveg=Elsoveg
    Hveg=Hatsoveg
    Balg=Balgomb
    Jobbg=Jobbgomb
    ManM=ManualM
    Ind1=ElsoInd
    Ind2=HatsoInd
    Veszk=Veszkor
    print "elso" ,Eveg
    print "hatso",Hveg
    print "Balg",Balg
    print "Jobbg",Jobbg
    print "ManM",ManM
    print "ind1",Ind1
    print "ind2",Ind2
    print "VesStop",Veszk
    
    if Jobbg==1 and Balg==1 and Veszk==0 and Hveg==0:
        Asztal()
    if Jobbg==1 and Balg==1 and Veszk==0 and Hveg==1 and ManM==1:
        AsztalM()
    if Hveg==1 and ManM==0 and Veszk==0:
        print "trigg"
    
 
class innput1:
    
    Elsoveg=0
    Hatsoveg=0
    Balgomb=0
    Jobbgomb=0
    ManualM=0
    ElsoInd=0
    HatsoInd=0
    Veszkor=0
    RawInp=0
    
    
    def __init__(self,RawInp='',PCF8574=0x39,in1=0,in2=0,in3=0,in4=0,in5=0,in6=0,in7=0,in8=0):
        self.InpData=''
        
        self.Inputs=[self.Elsoveg,self.Hatsoveg,self.Balgomb,self.Jobbgomb,self.ManualM,self.ElsoInd,self.HatsoInd,self.Veszkor]
        
    def GetInp(self,PCF8574):
        
        try:
            self.RawInp=PCF8574
            print self.RawInp
            self.InpData='{0:08b}'.format(self.RawInp)
            print "initiated"
            
        except:
            print    "Input_Mallfunction"
        
        print "get Inp function"
        tin1=self.InpData[0]
        tin2=self.InpData[1]
        tin3=self.InpData[2]
        tin4=self.InpData[3]
        tin5=self.InpData[4]
        tin6=self.InpData[5]
        tin7=self.InpData[6]
        tin8=self.InpData[7]
    
        #input value 
        self.in1=int(not(int(tin1)))
        self.in2=int(not(int(tin2)))
        self.in3=int(not(int(tin3)))
        self.in4=int(not(int(tin4)))
        self.in5=int(not(int(tin5)))
        self.in6=int(not(int(tin6)))
        self.in7=int(not(int(tin7)))
        self.in8=int(not(int(tin8)))
        
        #inputs by names
        self.Elsoveg=self.in1
        self.Hatsoveg=self.in2
        self.Balgomb=self.in3
        self.Jobbgomb=self.in4
        self.ManualM=self.in6
        self.ElsoInd=self.in7
        self.HatsoInd=self.in8
        self.Veszkor=self.in5
        
        self.InpData=''
        #input functions
        OutputHandler(self.Elsoveg,self.Hatsoveg,self.Balgomb,self.Jobbgomb,self.ManualM,self.ElsoInd,self.HatsoInd,self.Veszkor)
        
inp=innput1()
  
# Define a threaded callback function to run in another thread when events are detected  
def my_callback1(channel):  
    if GPIO.input(19):     # if port 25 == 1  
        print "Rising edge detected on 19"  
    else:  
        
        print "Falling edge detected on 19"  

def my_callback2(channel):  
    if GPIO.input(16):     # if port 25 == 1  
        inp.GetInp(b.read_byte(0x39))
        
        print "Rising edge detected on 16"  
    else:  
        inp.GetInp(b.read_byte(0x39))# if port 25 != 1 
        
        print "Falling edge detected on 16"  
  
# when a changing edge is detected on port 25, regardless of whatever   
# else is happening in the program, the function my_callback will be run  
  
GPIO.add_event_detect(19, GPIO.BOTH, callback=my_callback1, bouncetime=80)  
GPIO.add_event_detect(16, GPIO.BOTH, callback=my_callback2, bouncetime=80) 

    
print "Program will finish after 30 seconds or if you press CTRL+C\n"  
print "Make sure you have a button connected, pulled down through 10k resistor"  
print "to GND and wired so that when pressed it connects"  
print "GPIO port 25 (pin 22) to GND (pin 6) through a ~1k resistor\n"  
  
print "Also put a 100 nF capacitor across your switch for hardware debouncing"  
print "This is necessary to see the effect we're looking for"  
raw_input("Press Enter when ready\n>")  
  
try:  
    print "When pressed, you'll see: Rising Edge detected on 25"  
    print "When released, you'll see: Falling Edge detected on 25"  
    sleep(30)         # wait 30 seconds  
    print "Time's up. Finished!"  
  
finally:                   # this block will run no matter how the try block exits  
    GPIO.cleanup()         # clean up after yourself 