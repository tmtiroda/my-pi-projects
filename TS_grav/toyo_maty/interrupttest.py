#!/usr/bin/env python2.7  
# script by Alex Eames http://RasPi.tv  
# http://RasPi.tv/how-to-use-interrupts-with-python-on-the-raspberry-pi-and-rpi-gpio-part-3  
import RPi.GPIO as GPIO  
GPIO.setmode(GPIO.BCM)  

import smbus

b=smbus.SMBus(1)
PCF8574=0x39

def OutputHandler(Elsoveg,Hatsoveg,Balgomb,Jobbgomb,ManualM,ElsoInd,HatsoInd,Veszkor):
    print "outputs"
    Eveg=Elsoveg
    Hveg=Hatsoveg
    Balg=Balgomb
    Jobbg=Jobbgomb
    ManM=ManualM
    Ind1=ElsoInd
    Ind2=HatsoInd
    Veszk=Veszkor
    print "elso" ,Eveg
    print "hatso",Hveg
    print "Balg",Balg
    print "Jobbg",Jobbg
    print "ManM",ManM
    print "ind1",Ind1
    print "ind2",Ind2
    print "VesStop",Veszk
 
class innput1:
    
    Elsoveg=0
    Hatsoveg=0
    Balgomb=0
    Jobbgomb=0
    ManualM=0
    ElsoInd=0
    HatsoInd=0
    Veszkor=0
    RawInp=0
    
    
    def __init__(self,RawInp='',PCF8574=0x39,in1=0,in2=0,in3=0,in4=0,in5=0,in6=0,in7=0,in8=0):
        self.InpData=''
        
        self.Inputs=[self.Elsoveg,self.Hatsoveg,self.Balgomb,self.Jobbgomb,self.ManualM,self.ElsoInd,self.HatsoInd,self.Veszkor]
        
    def GetInp(self,PCF8574):
        
        try:
            self.RawInp=PCF8574
            print self.RawInp
            self.InpData='{0:08b}'.format(self.RawInp)
            print "initiated"
            
        except:
            print    "Input_Mallfunction"
        
        print "get Inp function"
        tin1=self.InpData[0]
        tin2=self.InpData[1]
        tin3=self.InpData[2]
        tin4=self.InpData[3]
        tin5=self.InpData[4]
        tin6=self.InpData[5]
        tin7=self.InpData[6]
        tin8=self.InpData[7]
    
        #input value 
        self.in1=int(not(int(tin1)))
        self.in2=int(not(int(tin2)))
        self.in3=int(not(int(tin3)))
        self.in4=int(not(int(tin4)))
        self.in5=int(not(int(tin5)))
        self.in6=int(not(int(tin6)))
        self.in7=int(not(int(tin7)))
        self.in8=int(not(int(tin8)))
        
        #inputs by names
        self.Elsoveg=self.in1
        self.Hatsoveg=self.in2
        self.Balgomb=self.in3
        self.Jobbgomb=self.in4
        self.ManualM=self.in6
        self.ElsoInd=self.in7
        self.HatsoInd=self.in8
        self.Veszkor=self.in5
        
        self.InpData=''
         
        #input functions
        OutputHandler(self.Elsoveg,self.Hatsoveg,self.Balgomb,self.Jobbgomb,self.ManualM,self.ElsoInd,self.HatsoInd,self.Veszkor)
        
inp=innput1()
# GPIO 23 & 17 set up as inputs, pulled up to avoid false detection.  
# Both ports are wired to connect to GND on button press.  
# So we'll be setting up falling edge detection for both  
GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_UP)  
  
# GPIO 24 set up as an input, pulled down, connected to 3V3 on button press  
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)  
  
# now we'll define two threaded callback functions  
# these will run in another thread when our events are detected  
def my_callback(channel): 
    # i2c address of PCF8574
    #inp.GetInp()
    inp.GetInp(b.read_byte(0x39))
    print "falling edge detected on 21"  
  
def my_callback2(channel):  
    print "falling edge detected on 23"  
  
print "Make sure you have a button connected so that when pressed"  
print "it will connect GPIO port 23 (pin 16) to GND (pin 6)\n"  
print "You will also need a second button connected so that when pressed"  
print "it will connect GPIO port 24 (pin 18) to 3V3 (pin 1)\n"  
print "You will also need a third button connected so that when pressed"  
print "it will connect GPIO port 17 (pin 11) to GND (pin 14)"  
raw_input("Press Enter when ready\n>")  
  
# when a falling edge is detected on port 17, regardless of whatever   
# else is happening in the program, the function my_callback will be run  
GPIO.add_event_detect(21, GPIO.FALLING, callback=my_callback, bouncetime=100)  
  
# when a falling edge is detected on port 23, regardless of whatever   
# else is happening in the program, the function my_callback2 will be run  
# 'bouncetime=300' includes the bounce control written into interrupts2a.py  
GPIO.add_event_detect(20, GPIO.FALLING, callback=my_callback2, bouncetime=100)  
  
try:  
    print "Waiting for rising edge on port 24"  
    GPIO.wait_for_edge(24, GPIO.RISING)  
    print "Rising edge detected on port 24. Here endeth the third lesson."  
  
except KeyboardInterrupt:  
    GPIO.cleanup()       # clean up GPIO on CTRL+C exit  
GPIO.cleanup()           # clean up GPIO on normal exit  