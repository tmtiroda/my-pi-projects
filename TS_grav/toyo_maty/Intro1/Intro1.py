#!/usr/bin/env python
#coding=utf8

import RPi.GPIO as GPIO  
  
GPIO.setmode(GPIO.BCM)
 

import smbus
import time
import binascii
import string
import OPP
import rsCom 
from rsCom import *


from threading import Timer


GPIO.setup(19, GPIO.IN, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_UP)

Oput=OPP.OPUT()

bus=smbus.SMBus(1)
PCF8574=0x39

rs485=rsCom.rscom485()
rs485.readRDY=1
rs232=rsCom.rscom232()

def Asztal():
    Oput.OPa4=1
    Oput.Write()
    print "asztal"
    inp.ControllBits=1
    inp.ControllBits2=1
    
def AsztalKi():
    Oput.OPa4=0
    Oput.Write()
    print "Asztalki"
    inp.ControllBits1=1

    
def Trig():
    print "Trigg"
    print inp.ControllBits
    if inp.ControllBits==1:
        Oput.OPb1=1
        Oput.Write()
        time.sleep(0.3)
        Oput.OPb1=0
        Oput.Write()
        print "TRIGGER"
        inp.ControllBits=0
        inp.Triggered=1
    
def ReadWt():
    if inp.Triggered==1: 
        rs485.readRDY=0
        rs485.rsread()
        j=0
        while rs485.readRDY==0 or j<100:
            time.sleep(0.05)
            j=j+1
        if inp2.Kival1==0 and inp2.Kival2==0 and rs485.ComunicationError==0:
            MarkEnable()
        
        if rs485.Noread1==0 and inp2.Kival1==0 and inp2.Kival2==1 and rs485.MarkNoread==0:
            MarkEnable()
        if rs485.Noread2==0 and inp2.Kival2==0 and inp2.Kival1==1 and rs485.MarkNoread==0:
            MarkEnable()
        if rs485.Noread2==0 and rs485.Noread1==0 and inp2.Kival1==1 and inp2.Kival2==1 and rs485.MarkNoread==0:
            MarkEnable()
        rs232.getStr(rs485.MarkString1,rs485.MarkString2)
        rs232.rswrite(rs485.Noread1,rs485.Noread2,rs485.MarkNoread,rs485.ComunicationError)
        print "nor1",rs485.Noread1
        print "nor2",rs485.Noread2
        print "marknr",rs485.MarkNoread
        print "comerr",rs485.ComunicationError
        
        rs485.readRDY=1
        
        inp.Triggered=0
    
        
def ReadWtMan():
    if inp.ControllBits==1: 
        rs485.readRDY=0
        if inp.Conf2==0 and inp2.Kival1==0 and inp2.Kival2==1:
            MarkEnable()
            M2._Entry1.focus()
        if inp.Conf1==0 and inp2.Kival2==0 and inp2.Kival1==1:
            MarkEnable()
            M1._Entry1.focus()
        if inp.Conf1==0 and inp.Conf2==0 and inp2.Kival1==1 and inp2.Kival2==1:
            MarkEnable()
            M2x._Entry1.focus()
            
        rs232.getStr(App.balold,App.jobbold)
        rs232.rswrite(inp.Conf2,inp.Conf1,0,0)
       
        rs485.readRDY=1
        App.balold=''
        App.jobbold=''
        inp.ControllBits=0


        
    
def Vakum():
    Oput.OPa5=1
    Oput.Write()
    
        
    
def Vakum0():
    Oput.OPa5=0
    Oput.Write()
    
def VakumKi():
    Oput.OPa5=0
    time.sleep(0.05)
    Oput.Write()
    Oput.OPa6=1
    Oput.Write()
    time.sleep(0.2)
    Oput.OPa6=0
    Oput.Write()
    
def VakumKi0():
    Oput.OPa6=0
    Oput.Write()

def VakumKiAuto():
    if inp.ControllBits1==1:
        Oput.OPa5=0
        time.sleep(0.05)
        Oput.Write()
        Oput.OPa6=1
        Oput.Write()
        time.sleep(0.2)
        Oput.OPa6=0
        Oput.Write()
        inp.ControllBits1=0

def Deff():
    Oput.Default()
    Oput.Write()
    inp2.ControllBits2=1
    print "Deffff"
    
    
def MarkEnable():
    Oput.OPb3=1
    Oput.Write()
    time.sleep(0.2)
    Oput.OPb3=0
    Oput.Write()
    
  
def OutputHandler(Elsoveg,Hatsoveg,Balgomb,Jobbgomb,ManualM,ElsoInd,HatsoInd,Veszkor):
    print "outputs"
    Eveg=Elsoveg
    Hveg=Hatsoveg
    Balg=Balgomb
    Jobbg=Jobbgomb
    ManM=ManualM
    Ind1=ElsoInd
    Ind2=HatsoInd
    Veszk=Veszkor
    
    print "ind1" ,Ind1 
    print "ind2" ,Ind2
    
    inp.Indu1=Ind1
    inp.Indu2=Ind2
    inp.ManMo=ManM
    inp.Jobbgo=Jobbg
    
    
    Grav.UpDat()
    print "man",ManM
    
    
    
    
    print "betöltve" "induktivs"
    
    rdy=0
    
    #automode eleje
#--------------------------------------------------------------------------------------------------

    #Automode asztal
    if Jobbg==1 and Balg==1 and ManM==0 and Veszk==0 and Hveg==0 and Eveg==1:
        inp2.Cstart=0
        Vakum()
        if inp2.Vakumk==1 and inp2.Kival1==1 and inp.Indu2==1 and inp2.Kival2==0:
            inp2.Cstart=0
            
            Asztal()
            print "someth"
        if inp2.Vakumk==1 and inp2.Kival2==1 and inp.Indu1==1 and inp2.Kival1==0:
            inp2.Cstart=0
            
            Asztal()
        if inp2.Vakumk==1 and inp2.Kival1==1 and inp.Indu2==1 and inp2.Kival2==1 and inp.Indu1==1:
            inp2.Cstart=0
           
            Asztal()
        if inp2.Vakumk==1 and inp2.Kival1==0 and inp2.Kival2==0:
            inp2.Cstart=0
            Asztal()
            
    if Jobbg==0 and Balg==0 and ManM==0 and Veszk==0 and Hveg==0 and Eveg==1:
        if inp.ControllBits==1:
            Vakum()
        if inp.ControllBits==0:
            Vakum0()
            
        
    #automode trigger
    if Hveg==1 and ManM==0 and Veszk==0 and inp2.Markrdy==1:
        print "k1 , K2",inp2.Kival1, inp2.Kival2 
        if inp2.Kival1==1 and inp2.Kival2==0:
            rs232.Wjobb=0
            rs232.Wbal=1
            print "wjobb", rs232.Wjobb
        if inp2.Kival1==0 and inp2.Kival2==1:
            rs232.Wjobb=1
            rs232.Wbal=0
            print "wjobb", rs232.Wjobb
        if inp2.Kival2==1 and inp2.Kival1==0:
            rs232.Wbal=0
            rs232.Wjobb=1
            print "wbal", rs232.Wbal
        if inp2.Kival2==0 and inp2.Kival1==0:
            rs232.Wbal=0
            rs232.Wjobb=0
            print "wbal", rs232.Wbal
        if inp2.Kival2==1 and inp2.Kival1==1:
            rs232.Wbal=1
            rs232.Wjobb=1
        inp2.Cstart=0    
        Trig()
        ReadWt()
        
        
    if Hveg==1 and ManM==0 and Veszk==0 and inp2.Markrdy==1 and inp2.Markerr==0 and inp2.Marking==1 and rs485.readRDY==1 and Eveg==0:
        inp2.Cstart=1
    if Hveg==1 and ManM==0 and Veszk==0 and inp2.Markrdy==0 and inp2.Marking==0 and Eveg==0:
        inp2.Cstart=0
    
    #uresjarat
    if Jobbg==0 and Balg==0 and ManM==0 and Veszk==0 and Hveg==0 and Eveg==1:
        inp2.Cstart=1
        inp.Autom=1
    if Jobbg==0 and Balg==0 and ManM==0 and Veszk==0 and Hveg==0 and Eveg==0:
        inp2.Cstart=0
        

    # automode vege       
    if Jobbg==1 and Balg==1 and ManM==0 and Veszk==0 and Hveg==1 and Eveg==0 and inp2.Markrdy==1 and inp2.Markerr==0 and inp2.Marking==1 and rs485.readRDY==1:
        AsztalKi()
        
    #auto mode vege vakum kifuj
    if Jobbg==0 and ManM==0 and Veszk==0 and Eveg==1:
        inp2.Cstart=1
        VakumKiAuto() 
     
    
    #manual asztal
    if Jobbg==1 and Balg==1 and ManM==1 and Veszk==0:
        VakumKi0()
        Vakum0()
        if Hveg==1:
            AsztalKi()
        if Eveg==1:
            Asztal()
            
   
        
    
    if Jobbg==0 and ManM==0 and Veszk==0 and Eveg==0 and inp2.Markerr==1:
        print "rs232 Error"
    if Balg==1 and ManM==1 and Veszk==0 and Eveg==1:
        Vakum()
        
#Manual Mode
#------------------------------------------------------------------------------------------
    #Automode asztal
    if Jobbg==1 and Balg==1 and ManM==1 and Veszk==0 and Hveg==0 and Eveg==1:
        inp2.Cstart=0
        Vakum()
        if inp2.Vakumk==1 and inp2.Kival1==1 and inp.Indu2==1 and inp2.Kival2==0:
            inp2.Cstart=0
            
            Asztal()
            print "someth"
        if inp2.Vakumk==1 and inp2.Kival2==1 and inp.Indu1==1 and inp2.Kival1==0:
            inp2.Cstart=0
            
            Asztal()
        if inp2.Vakumk==1 and inp2.Kival1==1 and inp.Indu2==1 and inp2.Kival2==1 and inp.Indu1==1:
            inp2.Cstart=0
           
            Asztal()
        if inp2.Vakumk==1 and inp2.Kival1==0 and inp2.Kival2==0:
            inp2.Cstart=0
            Asztal()
     
     
    if Jobbg==0 and Balg==0 and ManM==1 and Veszk==0 and Hveg==0 and Eveg==1:
        if inp.ControllBits==1:
            Vakum()
        if inp.ControllBits==0:
            Vakum0()
       
       
    if Hveg==1 and ManM==1 and Veszk==0 and inp2.Markrdy==1 and inp.Confrm==1:
        print "k1 , K2",inp2.Kival1, inp2.Kival2 
        if inp2.Kival1==1 and inp2.Kival2==0:
            rs232.Wjobb=0
            rs232.Wbal=1
            print "wjobb", rs232.Wjobb
        if inp2.Kival1==0 and inp2.Kival2==1:
            rs232.Wjobb=1
            rs232.Wbal=0
            print "wjobb", rs232.Wjobb
        if inp2.Kival2==1 and inp2.Kival1==0:
            rs232.Wbal=0
            rs232.Wjobb=1
            print "wbal", rs232.Wbal
        if inp2.Kival2==0 and inp2.Kival1==0:
            rs232.Wbal=0
            rs232.Wjobb=0
            print "wbal", rs232.Wbal
        if inp2.Kival2==1 and inp2.Kival1==1:
            rs232.Wbal=1
            rs232.Wjobb=1
        inp2.Cstart=0
        ReadWtMan()
        
        
        
    if Jobbg==0 and Balg==0 and ManM==1 and Veszk==0 and Hveg==0 and Eveg==1:
        if inp.ControllBits==1:
            Vakum()
        if inp.ControllBits==0:
            Vakum0()   
    
    
    #vissza alap hejzetbe veszkor utana
    if Veszk==1:
        Deff()
        print "veszkor", Veszk
    if ManM==1 and Veszk==0:
        pass
    if inp2.Cstart==1:
        Oput.OPa2=1
        Oput.Write()
    if inp2.Cstart==0:
        Oput.OPa2=0
        Oput.Write()
    
        
    
def OutputHandler2(MarkErr,MarkPos,MarkinG,VakumOK,Inp5,Inp6,Inp7,Inp8):
    print "outputs2"
    MarkE=MarkErr
    MarkR=MarkPos
    Marking=MarkinG
    VacOK=VakumOK
    inP5=Inp5
    inP6=Inp6
    inP7=Inp7
    inP8=Inp8
    print "markred", MarkR
    print "markp", Marking
    print "markERR",MarkE
    
    print "VacOK", VacOK
    if VacOK==1:
        inp2.Vakumk=1  
    if VacOK==0:
        inp2.Vakumk=0
    if MarkE==1:
        inp2.Markerr=1
    if MarkE==0:
        inp2.Markerr=0
    if MarkR==1:
        inp2.Markrdy=1
    if MarkR==0:
        inp2.Markrdy=0
    if Marking==1:
        inp2.Marking=1
    if Marking==0:
        inp2.Marking=0
    
class innput1:
    
    Elsoveg=0
    Hatsoveg=0
    Balgomb=0
    Jobbgomb=0
    ManualM=0
    ElsoInd=0
    HatsoInd=0
    Veszkor=0
    RawInp=0
    
    Elsoveg2=0
    Hatsoveg2=0
    Balgomb2=0
    Jobbgomb2=0
    ManualM2=0
    ElsoInd2=0
    HatsoInd2=0
    Veszkor2=0
    RawInp2=0
    
    
    ControllBits=0
    ControllBits1=0
    ControllBits2=0
    Triggered=0
    Indu1=0
    Indu2=0
    ManMo=0
    Jobbgo=0
    Autom=0
    Conf1=1
    Conf2=1
    Confrm=0
    
    

    def __init__(self,RawInp='',PCF8574=0x39,in1=0,in2=0,in3=0,in4=0,in5=0,in6=0,in7=0,in8=0):
        self.InpData=''
            
        self.Inputs=[self.Elsoveg,self.Hatsoveg,self.Balgomb,self.Jobbgomb,self.ManualM,self.ElsoInd,self.HatsoInd,self.Veszkor]
            
    def GetInp(self,pcfdata):
            
        try:
            self.RawInp=pcfdata
            print self.RawInp
            self.InpData='{0:08b}'.format(self.RawInp)
            print "initiated"
            print self.InpData
            
        except:
            print    "Input_Mallfunction"
        
        print "get Inp function"
        tin1=self.InpData[0]
        tin2=self.InpData[1]
        tin3=self.InpData[2]
        tin4=self.InpData[3]
        tin5=self.InpData[4]
        tin6=self.InpData[5]
        tin7=self.InpData[6]
        tin8=self.InpData[7]
    
        #input value 
        self.in1=int(not(int(tin1)))
        self.in2=int(not(int(tin2)))
        self.in3=int(not(int(tin3)))
        self.in4=int(not(int(tin4)))
        self.in5=int(not(int(tin5)))
        self.in6=int(not(int(tin6)))
        self.in7=int(not(int(tin7)))
        self.in8=int(not(int(tin8)))
        
        #inputs by names
        self.Elsoveg=self.in1
        self.Hatsoveg=self.in2
        self.Balgomb=self.in3
        self.Jobbgomb=self.in4
        self.ManualM=self.in6
        self.ElsoInd=self.in7
        self.HatsoInd=self.in8
        self.Veszkor=self.in5
        
        self.Elsoveg2=self.in1
        self.Hatsoveg2=self.in2
        self.Balgomb2=self.in3
        self.Jobbgomb2=self.in4
        self.ManualM2=self.in6
        self.ElsoInd2=self.in7
        self.HatsoInd2=self.in8
        self.Veszkor2=self.in5
        
        
        
        self.InpData=''
        #input functions
        OutputHandler(self.Elsoveg,self.Hatsoveg,self.Balgomb,self.Jobbgomb,self.ManualM,self.ElsoInd,self.HatsoInd,self.Veszkor)

class innput2:
        
    MarkinG=0
    MarkPos=0
    MarkErr=0
    VakumOK=0
    Inp5=0
    Inp6=0
    Inp7=0
    Inp8=0
    
    ControllBits=0
    ControllBits1=0
    ControllBits2=0
    Cstart=0
    
    Vakumk=0
    Marking=0
    Markerr=0
    Markrdy=0
    Kival1=0
    Kival2=0
    BalInp=''
    JobbInp=''
    
    def __init__(self,RawInp='',PCF8573=0x38,in1=0,in2=0,in3=0,in4=0,in5=0,in6=0,in7=0,in8=0):
        self.InpData=''
        
    def GetInp(self,pcfdata):
        
        try:
            self.RawInp=pcfdata
            print self.RawInp
            self.InpData='{0:08b}'.format(self.RawInp)
            print "initiated"
            print self.InpData
            
        except:
            print    "Input_Mallfunction"
        
        print "get Inp function"
        tin1=self.InpData[0]
        tin2=self.InpData[1]
        tin3=self.InpData[2]
        tin4=self.InpData[3]
        tin5=self.InpData[4]
        tin6=self.InpData[5]
        tin7=self.InpData[6]
        tin8=self.InpData[7]
    
        #input value 
        self.in1=int(not(int(tin1)))
        self.in2=int(not(int(tin2)))
        self.in3=int(not(int(tin3)))
        self.in4=int(not(int(tin4)))
        self.in5=int(not(int(tin5)))
        self.in6=int(not(int(tin6)))
        self.in7=int(not(int(tin7)))
        self.in8=int(not(int(tin8)))
        
        #inputs by names
        self.MarkErr=self.in1
        self.MarkPos=self.in2
        self.MarkinG=self.in3
        self.VakumOK=self.in4
        self.Inp5=self.in5
        self.Inp6=self.in6
        self.Inp7=self.in7
        self.Inp8=self.in8
    
        
        self.InpData=''
        #input functions
        
        
        OutputHandler2(self.MarkErr,self.MarkPos,self.MarkinG,self.VakumOK,self.Inp5,self.Inp6,self.Inp7,self.Inp8)
        OutputHandler(inp.Elsoveg2,inp.Hatsoveg2,inp.Balgomb2,inp.Jobbgomb2,inp.ManualM2,inp.ElsoInd2,inp.HatsoInd2,inp.Veszkor2)
        
inp=innput1()
inp2=innput2()

#IO here


#coding=utf8



import rpErrorHandler
from Tkinter import *
#------------------------------------------------------------------------------#
#                                                                              #
#                                   Confirm1                                   #
#                                                                              #
#------------------------------------------------------------------------------#
class Confirm1(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        kw['background'] = '#ff8000'
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_Confirm1_Key_Escape)
        self.bind('<KeyPress-Return>',self.on_Confirm1_Key_Return)
        self.baloldal = StringVar()
        self._Frame3 = Frame(self)
        self._Frame3.pack(expand='yes',fill='both',side='top')
        self._Label1 = Label(self._Frame3,activebackground='#ff8000'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#ff8000',text='Confirmation !')
        self._Label1.pack(expand='yes',fill='both',side='top')
        self._Label2 = Label(self._Frame3,activebackground='#ff8000'
            ,background='#ff8000',font=('Times New Roman CE', 18, '')
            ,highlightbackground='#ff8000',text='Ellenörzes!')
        self._Label2.pack(expand='yes',fill='both',side='top')
        self._Frame2 = Frame(self,background='#8080ff')
        self._Frame2.pack(expand='yes',fill='both',side='top')
        self._Label9 = Label(self._Frame2,activebackground='#8080ff'
            ,background='#8080ff')
        self._Label9.pack(pady='7',side='top')
        self._Label3 = Label(self._Frame2,activebackground='#ffffff'
            ,background='#ffffff',font=('Times New Roman CYR', 34, '')
            ,foreground='#ff0000',highlightbackground='#ffffff',relief='groove'
            ,textvariable=self.baloldal)
        self._Label3.pack(expand='yes',fill='both',ipady='10',side='top')
        self._Label8 = Label(self._Frame2,activebackground='#8080ff'
            ,background='#8080ff')
        self._Label8.pack(pady='7',side='bottom')
        self._Frame1 = Frame(self)
        self._Frame1.pack(padx='5',side='top')
        self._Frame4 = Frame(self._Frame1,background='#ff8000')
        self._Frame4.pack(expand='yes',fill='both',side='left')
        self._Label4 = Label(self._Frame4,activebackground='#8080ff',anchor='w'
            ,background='#ff8000',font=('Times New Roman Baltic', 12, '')
            ,highlightbackground='#8080ff',text='Press <esc> to modify')
        self._Label4.pack(expand='yes',fill='both',side='top')
        self._Label5 = Label(self._Frame4,activebackground='#8080ff',anchor='w'
            ,background='#ff8000',font=('Times New Roman CE', 12, '')
            ,highlightbackground='#8080ff',text='Nyomjon <esc> -et a javitashoz')
        self._Label5.pack(expand='yes',fill='both',side='top')
        self._Frame6 = Frame(self._Frame1,background='#ff8000')
        self._Frame6.pack(padx='2',side='left')
        self._Frame5 = Frame(self._Frame1,background='#ff8000')
        self._Frame5.pack(expand='yes',fill='both',side='left')
        self._Label6 = Label(self._Frame5,activebackground='#8080ff',anchor='e'
            ,background='#ff8000',font=('Times New Roman', 12, '')
            ,highlightbackground='#8080ff',text='Press <Enter> to acknowledge')
        self._Label6.pack(expand='yes',fill='both',side='top')
        self._Label7 = Label(self._Frame5,activebackground='#8080ff',anchor='e'
            ,background='#ff8000',font=('Times New Roman CE', 12, '')
            ,highlightbackground='#8080ff'
            ,text='Nyomjon <Enter> -t a jovahagyashoz')
        self._Label7.pack(side='top')
        self.attributes('-fullscreen', True)
        #
    def BalO(self,Bal):   
        self.baloldal.set(Bal)
        
        #Your code here
        #
       
    #
    #Start of event handler methods
    #
    #Start of event handler methods
    #


    def on_Confirm1_Key_Escape(self,Event=None):
        M1._Entry1.focus()
        App.balold=''
        inp.Conf1=1
        inp.Confrm=0
        #self.destroy()

    def on_Confirm1_Key_Return(self,Event=None):
        inp.Conf1=1
        inp.Confrm=0
        
        print len(self.baloldal.get())
        if len(App.balold)>0:
            print self.baloldal.get()
            print len(self.baloldal.get())
            inp.Conf1=0
            inp.Confrm=1
            
            self._Label1.configure(relief='sunken')
            self._Label1.configure(background='#00ff00')
            self._Label2.configure(relief='sunken')
            self._Label2.configure(background='#00ff00')

        
        #self.destroy()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                   Confirm2                                   #
#                                                                              #
#------------------------------------------------------------------------------#
class Confirm2(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        kw['background'] = '#ff8000'
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_Confirm1_Key_Escape)
        self.bind('<KeyPress-Return>',self.on_Confirm1_Key_Return)
        self.jobboldal = StringVar()
        self._Frame3 = Frame(self)
        self._Frame3.pack(expand='yes',fill='both',side='top')
        self._Label1 = Label(self._Frame3,activebackground='#ff8000'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#ff8000',text='Confirmation !')
        self._Label1.pack(expand='yes',fill='both',side='top')
        self._Label2 = Label(self._Frame3,activebackground='#ff8000'
            ,background='#ff8000',font=('Times New Roman CE', 18, '')
            ,highlightbackground='#ff8000',text='Ellenörzes!')
        self._Label2.pack(expand='yes',fill='both',side='top')
        self._Frame2 = Frame(self,background='#8080ff')
        self._Frame2.pack(expand='yes',fill='both',side='top')
        self._Label9 = Label(self._Frame2,activebackground='#8080ff'
            ,background='#8080ff')
        self._Label9.pack(pady='7',side='top')
        self._Label3 = Label(self._Frame2,activebackground='#ffffff'
            ,background='#ffffff',font=('Times New Roman CYR', 34, '')
            ,foreground='#ff0000',highlightbackground='#ffffff',relief='groove'
            ,textvariable=self.jobboldal)
        self._Label3.pack(expand='yes',fill='both',ipady='10',side='top')
        self._Label8 = Label(self._Frame2,activebackground='#8080ff'
            ,background='#8080ff')
        self._Label8.pack(pady='7',side='bottom')
        self._Frame1 = Frame(self)
        self._Frame1.pack(padx='5',side='top')
        self._Frame4 = Frame(self._Frame1,background='#ff8000')
        self._Frame4.pack(expand='yes',fill='both',side='left')
        self._Label4 = Label(self._Frame4,activebackground='#8080ff',anchor='w'
            ,background='#ff8000',font=('Times New Roman Baltic', 12, '')
            ,highlightbackground='#8080ff',text='Press <esc> to modify')
        self._Label4.pack(expand='yes',fill='both',side='top')
        self._Label5 = Label(self._Frame4,activebackground='#8080ff',anchor='w'
            ,background='#ff8000',font=('Times New Roman CE', 12, '')
            ,highlightbackground='#8080ff',text='Nyomjon <esc> -et a javitashoz')
        self._Label5.pack(expand='yes',fill='both',side='top')
        self._Frame6 = Frame(self._Frame1,background='#ff8000')
        self._Frame6.pack(padx='2',side='left')
        self._Frame5 = Frame(self._Frame1,background='#ff8000')
        self._Frame5.pack(expand='yes',fill='both',side='left')
        self._Label6 = Label(self._Frame5,activebackground='#8080ff',anchor='e'
            ,background='#ff8000',font=('Times New Roman', 12, '')
            ,highlightbackground='#8080ff',text='Press <Enter> to acknowledge')
        self._Label6.pack(expand='yes',fill='both',side='top')
        self._Label7 = Label(self._Frame5,activebackground='#8080ff',anchor='e'
            ,background='#ff8000',font=('Times New Roman CE', 12, '')
            ,highlightbackground='#8080ff'
            ,text='Nyomjon <Enter> -t a jovahagyashoz')
        self._Label7.pack(side='top')
        self.attributes('-fullscreen', True)
        #
    def BalO(self,Jobb):   
        self.jobboldal.set(Jobb)
        
        #Your code here
        #
       
    #
    #Start of event handler methods
    #
    #Start of event handler methods
    def on_Confirm1_Key_Escape(self,Event=None):
        M1._Entry1.focus()
        App.jobbold=''
        inp.Conf2=1
        inp.Confrm=0
        #self.destroy()
    
    #


    def on_Confirm1_Key_Return(self,Event=None):
        inp.Conf2=1
        inp.Confrm=0
        
        print len(self.jobboldal.get())
        if len(App.jobbold)>0:
            print self.jobboldal.get()
            print len(self.jobboldal.get())
            inp.Conf2=0
            inp.Confrm=1
            
            self._Label1.configure(relief='sunken')
            self._Label1.configure(background='#00ff00')
            self._Label2.configure(relief='sunken')
            self._Label2.configure(background='#00ff00')
        #self.destroy()
    #
    #Start of non-Rapyd user code
    #
pass #end of code
#------------------------------------------------------------------------------#
#                                                                              #
#                                  Confirm2x                                   #
#                                                                              #
#------------------------------------------------------------------------------#
class Confirm2x(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_Confirm2x_Key_Escape)
        self.bind('<KeyPress-Return>',self.on_Confirm2x_Key_Return)
        self.baloldal = StringVar()
        self.jobboldal = StringVar()
        self._Frame2 = Frame(self)
        self._Frame2.pack(expand='yes',fill='both',side='top')
        self._Label1 = Label(self._Frame2,activebackground='#ff8000'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#ff8000',text='Confirmation !')
        self._Label1.pack(expand='yes',fill='both',side='top')
        self._Label2 = Label(self._Frame2,activebackground='#ff8000'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#ff8000',text='Ellenörzés!')
        self._Label2.pack(expand='yes',fill='both',side='top')
        self._Frame1 = Frame(self)
        self._Frame1.pack(side='top')
        self._Label8 = Label(self._Frame1,activebackground='#ffffff'
            ,background='#ffffff',font=('Times New Roman', 40, '')
            ,foreground='#ff0000',highlightbackground='#ffffff',relief='groove'
            ,textvariable=self.baloldal)
        self._Label8.pack(expand='yes',fill='both',ipadx='300',pady='5'
            ,side='top')
        self._Label7 = Label(self._Frame1,activebackground='#ffffff'
            ,background='#ffffff',font=('Times New Roman', 40, '')
            ,foreground='#ff0000',highlightbackground='#ffffff',relief='groove'
            ,textvariable=self.jobboldal)
        self._Label7.pack(expand='yes',fill='both',ipadx='300',pady='5'
            ,side='top')
        self._Frame3 = Frame(self)
        self._Frame3.pack(side='top')
        self._Frame4 = Frame(self._Frame3)
        self._Frame4.pack(expand='yes',fill='both',side='left')
        self._Label3 = Label(self._Frame4,activebackground='#8080ff',anchor='w'
            ,background='#ff8000',font=('Times New Roman', 15, '')
            ,highlightbackground='#8080ff',text='Press <esc> to modify')
        self._Label3.pack(padx='20',pady='20',side='top')
        self._Label4 = Label(self._Frame4,activebackground='#8080ff',anchor='w'
            ,background='#ff8000',font=('Times New Roman', 15, '')
            ,highlightbackground='#8080ff',text='Nyomjon <esc> -et a javításhoz')
        self._Label4.pack(padx='20',pady='20',side='top')
        self._Frame5 = Frame(self._Frame3)
        self._Frame5.pack(expand='yes',fill='both',side='left')
        self._Label5 = Label(self._Frame5,activebackground='#8080ff',anchor='e'
            ,background='#ff8000',font=('Times New Roman', 15, '')
            ,highlightbackground='#8080ff',text='Press <Enter> to acknowledge')
        self._Label5.pack(padx='20',pady='20',side='top')
        self._Label6 = Label(self._Frame5,activebackground='#8080ff',anchor='e'
            ,background='#ff8000',font=('Times New Roman', 15, '')
            ,highlightbackground='#8080ff'
            ,text='Nyomjon <Enter> -t a jóváhagyáshoz')
        self._Label6.pack(padx='20',pady='20',side='top')
        #
        #Your code here
        #
        self.attributes('-fullscreen', True)
        
        self._Label1.configure(relief='raised')
        
    def UD():
        self._Label1.configure(background='#ff8000')
        self._Label2.configure(relief='raised')
        self._Label2.configure(background='#ff8000')
        self._Label1.configure(relief='raised')
        
    def Get(self,bal,jobb):
        self.baloldal.set(bal)
        self.jobboldal.set(jobb)
        
        
    
    #
    #Start of event handler methods
    #


    def on_Confirm2x_Key_Escape(self,Event=None):
        M2x._Entry1.focus()
        App.jobbold=''
        App.balold=''
        inp.Conf2=1
        inp.Conf1=1
        inp.Confrm=0
        #self.destroy()
        
    def on_Confirm2x_Key_Return(self,Event=None):
        inp.Conf2=1
        inp.Conf1=1
        inp.Confrm=0
        
        print len(self.jobboldal.get())
        if len(App.jobbold)>0 and len(App.balold)>0:
            
            inp.Conf2=0
            inp.Conf1=0
            inp.Confrm=1
            
            self._Label1.configure(relief='sunken')
            self._Label1.configure(background='#00ff00')
            self._Label2.configure(relief='sunken')
            self._Label2.configure(background='#00ff00')
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                    Error                                     #
#                                                                              #
#------------------------------------------------------------------------------#
class Error(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self._Label1 = Label(self,background='#808080'
            ,font=('Times New Roman', 30, 'bold'),foreground='#ff0000'
            ,text='EMERGENCY STOP ')
        self._Label1.pack(expand='yes',fill='both',side='left')
        #
        #Your code here
        #
        self.attributes('-fullscreen', True)
    #
    #Start of event handler methods
    #

    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                    Grav1                                     #
#                                                                              #
#------------------------------------------------------------------------------#
class Grav1(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #
        
        import time
        

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_Grav1_Key_Escape)
        self._Frame3 = Frame(self)
        self._Frame3.pack(expand='yes',fill='both',side='top')
        self._Label1 = Label(self._Frame3,background='#c0c0c0'
            ,bitmap='@TS_bal.XBM',relief='raised')
        self._Label1.pack(expand='yes',fill='both',side='top')
        self._Label4 = Label(self._Frame3,background='#c0c0c0'
            ,bitmap='@TS_jobb.XBM',relief='raised')
        self._Label4.pack(expand='yes',fill='both',side='top')
        self._Label4.bind('<Any-ButtonPress>',self._on_Label3_Button__N)
        self._Label4.bind('<ButtonRelease-1>',self._on_Label3_ButRel_1)
        self._Frame2 = Frame(self,background='#0080c0')
        self._Frame2.pack(expand='yes',fill='both',side='top')
        self._Label2 = Label(self._Frame2,background='#8080ff'
            ,font=('Times New Roman', 12, '')
            ,text='Work Select \n Munkadarab kiválasztás')
        self._Label2.pack(expand='yes',fill='both',side='left')
        self._Label2.bind('<ButtonRelease-1>',self._on_Label2_ButRel_1)
        self._Frame1 = Frame(self)
        self._Frame1.pack(expand='yes',fill='both',side='top')
        self._Button1 = Button(self._Frame1,command=self._on__Button1_command)
        self._Button1.pack(expand='yes',fill='both',side='left')
        #
        #Your code here
        #
        
        
        self.attributes('-fullscreen', True)
          
    def UpDat(self):
        if inp.Indu2==1:
            
            self._Label1.configure(relief='sunken')
            self._Label1.configure(background='#00ff00')
            
        else :
            self._Label1.configure(relief='raised')
            self._Label1.configure(background='#c0c0c0')
        
        if inp.Indu1==1:
            self._Label4.configure(relief='sunken')
            self._Label4.configure(background='#00ff00')
        elif inp.Indu1==0:
            self._Label4.configure(relief='raised')
            self._Label4.configure(background='#c0c0c0')
        else:
           pass
        
        
    #
    #Start of event handler methods
    #


    def _on_Label2_ButRel_1(self,Event=None):
        if inp.ManMo==1:
            ManM.focus()
        else:
            pass
        #self.after(200,self.destroy())

    def _on_Label3_ButRel_1(self,Event=None):
        pass

    def _on_Label3_Button__N(self,Event=None):
        pass

    def _on__Button1_command(self,Event=None):
        if inp.Jobbgo==1:
            ManPass.focus()
        #self.after(200,self.destroy())

    def on_Grav1_Key_Escape(self,Event=None):
        sys.exit()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                    Intro1                                    #
#                                                                              #
#------------------------------------------------------------------------------#
class Intro1(Frame):
    balold=''
    jobbold=''
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #
        
        apply(Frame.__init__,(self,Master),kw)
        self._Label1 = Label(self,bitmap='@tmtlogo.XBM')
        self._Label1.pack(expand='yes',fill='both',side='top')
        #
     
        #Your code here
        #

            
        self.after(1300, lambda: Grav.focus())
        #self.after(1400, lambda: self.destroy())
        
        
        #self.after(1000, lambda: Fcuser().deiconify())
    
    #
    #Start of event handler methods
    #

    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                               InvalidPassword                                #
#                                                                              #
#------------------------------------------------------------------------------#
class InvalidPassword(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<Any-ButtonPress>',self.on_InvalidPassword_Button__N)
        self.bind('<KeyPress-Escape>',self.on_InvalidPassword_Key_Escape)
        self._Label1 = Label(self,font=('Times New Roman', 30, '')
            ,foreground='#ff0000',text='Invalid Password')
        self._Label1.pack(expand='yes',fill='both',side='left')
        #
        #Your code here
        #
        self.attributes('-fullscreen', True)
    #
    #Start of event handler methods
    #


    def _on_Button1_Button__N(self,Event=None):
        ManPass.focus()

    def on_InvalidPassword_Button__N(self,Event=None):
        ManPass.focus()

    def on_InvalidPassword_Key_Escape(self,Event=None):
        escape()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                     Man1                                     #
#                                                                              #
#------------------------------------------------------------------------------#
class Man1(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.Keyb = StringVar()
        self._Frame2 = Frame(self)
        self._Frame2.pack(expand='yes',fill='both',side='left')
        self._Button1 = Button(self._Frame2,bitmap='@vissza800.XBM'
            ,command=self._on__Button1_command)
        self._Button1.pack(anchor='w',expand='yes',fill='y',side='left')
        self._Frame1 = Frame(self)
        self._Frame1.pack(expand='yes',fill='both',side='left')
        self._Label1 = Label(self._Frame1,font=('Times New Roman', 28, '')
            ,foreground='#ff8000',text='Manual Input (Left)')
        self._Label1.pack(pady='20',side='top')
        self._Label2 = Label(self._Frame1,font=('Times New Roman', 28, '')
            ,foreground='#ff8000',text='Kézi bevitel (Bal)')
        self._Label2.pack(pady='5',side='top')
        self._Label3 = Label(self._Frame1,bitmap='@bal.XBM')
        self._Label3.pack(pady='30',side='top')
        self._Label4 = Label(self._Frame1,font=('Times New Roman', 28, '')
            ,foreground='#ff0000',textvariable=self.Keyb)
        self._Label4.pack(fill='both',side='top')
        self._Entry1 = Entry(self._Frame1,font=('Times New Roman', 21, '')
            ,justify='center',takefocus=1,textvariable=self.Keyb)
        self._Entry1.pack(expand='yes',fill='x',pady='50',side='bottom')
        self._Entry1.bind('<KeyPress-Return>',self._on_Entry1_Key_Return)
        self._Frame3 = Frame(self)
        self._Frame3.pack(expand='yes',fill='both',side='left')
        #
        #Your code here
        #
        self._Entry1.focus()
        self.attributes('-fullscreen', True)
    #
    #Start of event handler methods
    #

    
    def _on_Entry1_Key_Return(self,Event=None):
        
        App.balold=self.Keyb.get()
        C1.BalO(App.balold)
       
        self._Entry1.delete(0, END)
        self._Entry1.insert(0, "")
        C1.focus()
        #self.after(200,self.destroy())
        print len(self.Keyb.get())

    def _on__Button1_command(self,Event=None):
        ManM.focus()
        #self.after(200,self.destroy())
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                     Man2                                     #
#                                                                              #
#------------------------------------------------------------------------------#
class Man2(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.Keyb = StringVar()
        self._Frame2 = Frame(self)
        self._Frame2.pack(expand='yes',fill='both',side='left')
        self._Button1 = Button(self._Frame2,bitmap='@vissza800.XBM'
            ,command=self._on__Button1_command)
        self._Button1.pack(anchor='w',expand='yes',fill='y',side='right')
        self._Frame1 = Frame(self)
        self._Frame1.pack(expand='yes',fill='both',side='left')
        self._Label3 = Label(self._Frame1,font=('Times New Roman TUR', 28, '')
            ,foreground='#ff8000',text='Manual Input (Right)')
        self._Label3.pack(pady='20',side='top')
        self._Label4 = Label(self._Frame1,font=('Times New Roman', 28, '')
            ,foreground='#ff8000',text='Kézi bevitel (jobb)')
        self._Label4.pack(pady='5',side='top')
        self._Label2 = Label(self._Frame1,bitmap='@jobb.XBM')
        self._Label2.pack(pady='30',side='top')
        self._Label1 = Label(self._Frame1,font=('Times New Roman', 28, '')
            ,foreground='#ff0000',textvariable=self.Keyb)
        self._Label1.pack(fill='both',side='top')
        self._Entry1 = Entry(self._Frame1,font=('Times New Roman', 22, '')
            ,justify='center',takefocus=1,textvariable=self.Keyb)
        self._Entry1.pack(fill='x',pady='50',side='top')
        self._Entry1.bind('<KeyPress-Return>',self._on_Entry1_Key_Return)
        self._Frame3 = Frame(self)
        self._Frame3.pack(expand='yes',fill='both',side='left')

        #Your code here
        #
        
        self.attributes('-fullscreen', True)
        
        
    #
    #Start of event handler methods
    #


    def _on_Entry1_Key_Return(self,Event=None):
       
        App.jobbold=self.Keyb.get()
        C2.BalO(App.jobbold)
       
        self._Entry1.delete(0, END)
        self._Entry1.insert(0, "")
        C2.focus()
        #self.after(200,self.destroy())

    def _on__Button1_command(self,Event=None):
        ManM.focus()
        #self.after(200,self.destroy())
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                    Man2x                                     #
#                                                                              #
#------------------------------------------------------------------------------#
class Man2x(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #
  

        apply(Toplevel.__init__,(self,Master),kw)
        self.Echo = StringVar()
        self.Echo2 = StringVar()
        self.EchoCont1 = StringVar()
        self.EchoCont2 = StringVar()
        self._Frame6 = Frame(self)
        self._Frame6.pack(expand='yes',fill='both',side='left')
        self._Button1 = Button(self._Frame6,bitmap='@vissza800.XBM'
            ,command=self._on__Button1_command)
        self._Button1.pack(anchor='w',expand='yes',fill='y',side='left')
        self._Frame5 = Frame(self)
        self._Frame5.pack(side='left')
        self._Frame4 = Frame(self)
        self._Frame4.pack(expand='yes',fill='both',side='left')
        self._Frame3 = Frame(self._Frame5)
        self._Frame3.pack(side='top')
        self._Label5 = Label(self._Frame3,bitmap='@TS_kbal.XBM')
        self._Label5.pack(side='top')
        self._Label1 = Label(self._Frame3,font=('Times New Roman', 15, '')
            ,foreground='#800000',text='Left\nBaloldal')
        self._Label1.pack(expand='yes',fill='both',ipadx='100',side='top')
        self._Entry1 = Entry(self._Frame3,font=('Times New Roman', 24, '')
            ,justify='center',textvariable=self.Echo)
        self._Entry1.pack(expand='yes',fill='both',side='top')
        self._Entry1.bind('<KeyRelease>',self._on_Entry1_KeyRel)
        self._Entry1.bind('<KeyRelease-Return>',self._on_Entry1_Key_Return)
        self._Frame2 = Frame(self._Frame5)
        self._Frame2.pack(expand='yes',fill='both',side='top')
        self._Label3 = Label(self._Frame2,font=('Times New Roman CE', 28, '')
            ,foreground='#ff0000',textvariable=self.EchoCont1)
        self._Label3.pack(expand='yes',fill='both',pady='5',side='top')
        self._Label4 = Label(self._Frame2,font=('Times New Roman CE', 28, '')
            ,foreground='#ff0000',textvariable=self.EchoCont2)
        self._Label4.pack(expand='yes',fill='both',pady='5',side='bottom')
        self._Frame1 = Frame(self._Frame5)
        self._Frame1.pack(side='top')
        self._Entry2 = Entry(self._Frame1,font=('Times New Roman', 24, '')
            ,justify='center',textvariable=self.Echo2)
        self._Entry2.pack(expand='yes',fill='both',side='top')
        self._Entry2.bind('<KeyRelease>',self._on_Entry2_KeyRel)
        self._Entry2.bind('<KeyRelease-Return>',self._on_Entry2_Key_Return)
        self._Label2 = Label(self._Frame1,anchor='s'
            ,font=('Times New Roman', 15, ''),foreground='#800000'
            ,text='Jobboldal\nRight')
        self._Label2.pack(expand='yes',fill='both',ipadx='100',side='top')
        self._Label6 = Label(self._Frame1,bitmap='@TS_kjobb.XBM')
        self._Label6.pack(side='bottom')
        #
        #Your code here
        #
        self.attributes('-fullscreen', True)
        
        
        
    #
    #Start of event handler methods
    #


    def _on_Entry1_KeyRel(self,Event=None):
    #    self.EchoCont1.set(self.Echo.get())
        pass

    def _on_Entry1_Key_Return(self,Event=None):
        C2x.UD()
        App.balold=''
        
        App.balold=self.Echo.get()
        self.EchoCont1.set(self.Echo.get())
        #self.EchoCont1.set(self.ECHO1.get())
        if self.Echo.get()!='':
            Mark.set(1)
            
        self._Entry1.delete(0, END)
        self._Entry1.insert(0, "")
        
        if MarkRdy.get()!=3:
            M2x._Entry2.focus()
        if MarkRdy.get()==3:
            MarkRdy.set(0)
            #self.destroy()
        if App.jobbold!='':
            C2x.Get(App.balold,App.jobbold)
            
            
        
       

    def _on_Entry2_KeyRel(self,Event=None):
        #self.EchoCont2.set(self.Echo2.get())
        pass

    def _on_Entry2_Key_Return(self,Event=None):
        App.jobbold=''

        App.jobbold=self.Echo2.get()
        self.EchoCont2.set(self.Echo2.get())
        if self.Echo2.get()!='':
            Mark2.set(1)
        
        self._Entry2.delete(0, END)
        self._Entry2.insert(0, "")
        
        if MarkRdy.get()!=3:
            M2x._Entry1.focus()
        if MarkRdy.get()==3:
            MarkRdy.set(0)
            #self.destroy()
        if App.balold!='':
            C2x.Get(App.balold,App.jobbold)
            

    def _on__Button1_command(self,Event=None):
        ManM.focus()
        #self.destroy()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                   ManMode                                    #
#                                                                              #
#------------------------------------------------------------------------------#
class ManMode(Toplevel):
    
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #
        import time
        self.Variable=IntVar()   

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_ManMode_Key_Escape)
        self._Frame2 = Frame(self)
        self._Frame2.pack(expand='yes',fill='both',side='left')
        self._Button1 = Button(self._Frame2,bitmap='@vissza800.XBM'
            ,command=self._on__Button1_command)
        self._Button1.pack(anchor='e',expand='yes',fill='y',side='right')
        self._Frame4 = Frame(self,background='#c0c0c0')
        self._Frame4.pack(expand='yes',fill='both',side='left')
        self._Label4 = Label(self._Frame4,background='#c0c0c0'
            ,font=('Times New Roman', 20, ''),foreground='#ff0000'
            ,relief='groove',takefocus=1,text='Work selection')
        self._Label4.pack(pady='20',side='top')
        self._Label1 = Label(self._Frame4,background='#c0c0c0'
            ,font=('Times New Roman', 20, ''),foreground='#ff0000'
            ,relief='groove',text='Munkadarab kiválasztás')
        self._Label1.pack(pady='10',side='top')
        self._Label2 = Label(self._Frame4,background='#c0c0c0'
            ,bitmap='@TS_bal.XBM',relief='raised')
        self._Label2.pack(anchor='n',side='top')
        self._Label2.bind('<Any-ButtonPress>',self._on_Label2_Button__N)
        self._Label2.bind('<ButtonRelease-1>',self._on_Label2_ButRel_1)
        self._Label3 = Label(self._Frame4,background='#c0c0c0'
            ,bitmap='@TS_jobb.XBM',relief='raised')
        self._Label3.pack(anchor='s',side='bottom')
        self._Label3.bind('<Any-ButtonPress>',self._on_Label3_Button__N)
        self._Label3.bind('<ButtonRelease-1>',self._on_Label3_ButRel_1)
        self._Frame3 = Frame(self)
        self._Frame3.pack(expand='yes',fill='both',side='left')
        self._Button2 = Button(self._Frame3,bitmap='@elore800.XBM'
            ,command=self._on__Button2_command)
        self._Button2.pack(anchor='w',expand='yes',fill='y',side='left')
        self._Button2.bind('<Any-ButtonRelease>',self._on_Button2_ButRel__N)
        #
        self.attributes('-fullscreen', True)
        #Your code here
        if a==1:
            self._Label2.configure(relief='sunken')
            self._Label2.configure(background='#00ff00')
            
        else :
            self._Label2.configure(relief='raised')
            self._Label2.configure(background='#c0c0c0')
        
        if b==1:
            self._Label3.configure(relief='sunken')
            self._Label3.configure(background='#00ff00')
        elif b==0:
            self._Label3.configure(relief='raised')
            self._Label3.configure(background='#c0c0c0')
        else:
            pass
         
    #
    #Start of event handler methods
    #


    def _on_Button2_ButRel__N(self,Event=None):
        pass
        

    def _on_Frame1_Key_Escape(self,Event=None):
        escape()

    def _on_Frame2_Key_Escape(self,Event=None):
        escape()

    def _on_Frame3_Key_Escape(self,Event=None):
        escape()

    def _on_Label2_ButRel_1(self,Event=None):
        pass

    def _on_Label2_Button__N(self,Event=None):
        # Beragadó billentyű fölső
        FlipBal.get() 
        if a==1:
            inp2.Kival1=1
            self._Label2.configure(relief='sunken')
            self._Label2.configure(background='#00ff00')
            
        else:
            inp2.Kival1=0
            self._Label2.configure(relief='raised')
            self._Label2.configure(background='#c0c0c0')
       

    def _on_Label3_ButRel_1(self,Event=None):
        pass
        

    def _on_Label3_Button__N(self,Event=None):
        #Beragadó billentyű alsó
        FlipJobb.get()
        if b==1:
            inp2.Kival2=1
            self._Label3.configure(relief='sunken')
            self._Label3.configure(background='#00ff00')
        else:
            inp2.Kival2=0
            self._Label3.configure(relief='raised')
            self._Label3.configure(background='#c0c0c0')
        

    def _on__Button1_command(self,Event=None):
        Grav.focus()
        #self.after(200,self.destroy())

    def _on__Button2_command(self,Event=None):
        # Ki választja a megfelelő kézi beviteli képernyőt
        if a==1 and b==0:
            #self.after(200,self.destroy())
            M1._Entry1.focus()
        elif a==0 and b==1:
            #self.after(200,self.destroy())
            M2._Entry1.focus()
        elif a==1 and b==1:
            #self.after(200,self.destroy())
            M2x._Entry1.focus()
        else:
            pass

    def on_ManMode_Key_Escape(self,Event=None):
        escape()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                   Manual1                                    #
#                                                                              #
#------------------------------------------------------------------------------#
class Manual1(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_Manual1_Key_Escape)
        self._Frame2 = Frame(self)
        self._Frame2.pack(side='top')
        self._Frame2.bind('<KeyPress-Escape>',self._on_Frame2_Key_Escape)
        self._Label4 = Label(self._Frame2,background='#c0c0c0',relief='raised'
            ,text='Table (UP / DOWN) \n Asztal (FÖL / LE)')
        self._Label4.pack(anchor='n',ipadx='20',pady='30',side='left')
        self._Label4.bind('<Any-ButtonPress>',self._on_Label2_Button__N)
        self._Frame1 = Frame(self)
        self._Frame1.pack(side='top')
        self._Frame1.bind('<KeyPress-Escape>',self._on_Frame1_Key_Escape)
        self._Label3 = Label(self._Frame1,background='#c0c0c0',relief='raised'
            ,text='Vacum select \n Vákum szivattyú')
        self._Label3.pack(ipadx='20',pady='30',side='left')
        self._Label3.bind('<Any-ButtonPress>',self._on_Label3_Button__N)
        self._Frame4 = Frame(self)
        self._Frame4.pack(side='top')
        self._Frame4.bind('<KeyPress-Escape>',self._on_Frame4_Key_Escape)
        self._Label5 = Label(self._Frame4,background='#c0c0c0',relief='raised'
            ,text='Camera select \n Kamera kiválaszt')
        self._Label5.pack(ipadx='20',pady='30',side='left')
        self._Label5.bind('<Any-ButtonPress>',self._on_Label5_Button__N)
        self._Frame5 = Frame(self)
        self._Frame5.pack(side='top')
        self._Label6 = Label(self._Frame5,background='#c0c0c0',relief='raised'
            ,text='Engrave select \n Gravírozó kiválaszt')
        self._Label6.pack(ipadx='20',pady='30',side='top')
        self._Label6.bind('<Any-ButtonPress>',self._on_Label6_Button__N)
        #
        self.attributes('-fullscreen', True)
      
        #Your code here
        #
    #
    #Start of event handler methods
    #


    def _on_Frame1_Key_Escape(self,Event=None):
        escape()

    def _on_Frame2_Key_Escape(self,Event=None):
        escape()

    def _on_Frame3_Key_Escape(self,Event=None):
        escape()

    def _on_Frame4_Key_Escape(self,Event=None):
        escape()

    def _on_Label2_Button__N(self,Event=None):
         #Beragadó billentyű Pneumatika
        flipPneu.get()
        if PneuKiv==1:
            self._Label4.configure(relief='sunken')
            self._Label4.configure(background='#00ff00')
        else:
            self._Label4.configure(relief='raised')
            self._Label4.configure(background='#c0c0c0')
        #self.update()
        

    def _on_Label3_Button__N(self,Event=None):
        #Beragadó billentyű Vákum
        flipVacum.get()
        if VacumKiv==1:
            self._Label3.configure(relief='sunken')
            self._Label3.configure(background='#00ff00')
        else:
            self._Label3.configure(relief='raised')
            self._Label3.configure(background='#c0c0c0')
        #self.update()
        

    def _on_Label5_Button__N(self,Event=None):
        #Beragadó billentyű Kamera
        flipKamera.get()
        if KameraKiv==1:
            self._Label5.configure(relief='sunken')
            self._Label5.configure(background='#00ff00')
        else:
            self._Label5.configure(relief='raised')
            self._Label5.configure(background='#c0c0c0')
        #self.update()
        

    def _on_Label6_Button__N(self,Event=None):
        #Beragadó billentyű 
        flipGravir.get()
        if GravirKiv==1:
            self._Label6.configure(relief='sunken')
            self._Label6.configure(background='#00ff00')
        else:
            self._Label6.configure(relief='raised')
            self._Label6.configure(background='#c0c0c0')
        #self.update()
        

    def on_Manual1_Key_Escape(self,Event=None):
        #Beragadó billentyű alsó
        escape()
        #self.destroy()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                ManualPassword                                #
#                                                                              #
#------------------------------------------------------------------------------#
class ManualPassword(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_ManualPassword_Key_Escape)
        self.Password = StringVar()
        self._Label1 = Label(self)
        self._Label1.pack(side='top')
        self._Entry1 = Entry(self,font=('Times New Roman', 28, '')
            ,justify='center',show='X',takefocus=1,textvariable=self.Password)
        self._Entry1.pack(side='top')
        self._Entry1.bind('<KeyPress-Return>',self._on_Entry1_Key_Return)
        self._Entry1.focus_set()
        #Your code here
        #
        self.attributes('-fullscreen', True)
    #
    #Start of event handler methods
    #
        

    def _on_Entry1_Key_Return(self,Event=None):
        Pass=self.Password.get()
        self._Entry1.delete(0, END)
        self._Entry1.insert(0,'')
        if Pass==Password:
            Manu1.focus()
            #self.after(200,self.destroy())
        else:
            InvPass.focus()
            
            
           

    def on_ManualPassword_Key_Escape(self,Event=None):
        escape()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                  ReadError                                   #
#                                                                              #
#------------------------------------------------------------------------------#
class ReadError(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self._Frame2 = Frame(self,background='#ff0000')
        self._Frame2.pack(expand='yes',fill='y',side='left')
        self._Label1 = Label(self._Frame2,background='#ff0000'
            ,bitmap='@readerror800.XBM')
        self._Label1.pack(expand='yes',fill='both',side='left')
        self._Frame1 = Frame(self)
        self._Frame1.pack(side='left')
        self._Frame4 = Frame(self._Frame1)
        self._Frame4.pack(expand='yes',fill='both',side='top')
        self._Label4 = Label(self._Frame4,font=('Times New Roman', 28, '')
            ,text=1)
        self._Label4.pack(anchor='n',expand='yes',fill='both',padx='10'
            ,side='top')
        self._Label2 = Label(self._Frame4,bitmap='@bal.XBM'
            ,font=('Times New Roman', 30, ''),text=1)
        self._Label2.pack(anchor='n',expand='yes',fill='both',padx='10'
            ,pady='30',side='top')
        self._Frame3 = Frame(self._Frame1)
        self._Frame3.pack(expand='yes',fill='both',side='top')
        self._Label3 = Label(self._Frame3,bitmap='@jobb.XBM'
            ,font=('Times New Roman', 30, ''),text=2)
        self._Label3.pack(anchor='s',expand='yes',fill='both',padx='10'
            ,pady='30',side='top')
        self._Label5 = Label(self._Frame3,font=('Times New Roman', 28, '')
            ,text=2)
        self._Label5.pack(anchor='s',expand='yes',fill='both',padx='10'
            ,side='top')
        #
        #Your code here
        #
        self.attributes('-fullscreen', True)
    #
    #Start of event handler methods
    #

    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---

try:
    #--------------------------------------------------------------------------#
    # User code should go after this comment so it is inside the "try".        #
    #     This allows rpErrorHandler to gain control on an error so it         #
    #     can properly display a Rapyd-aware error message.                    #
    #--------------------------------------------------------------------------#
    
    Password='iddqd'
    Pass='0'
    keyboard='NULL'
    a=0
    b=0
    PneuKiv=0
    VacumKiv=0
    KameraKiv=0
    GravirKiv=0    

    def flipPneum(*args):
        global PneuKiv
        PneuKiv=int(not PneuKiv) 
        
    def flipVacumm(*args):
        global VacumKiv
        VacumKiv=int(not VacumKiv)
        
    def flipKameram(*args):
        global KameraKiv
        KameraKiv=int(not KameraKiv)
        
    def flipGravirm(*args):
        global GravirKiv
        GravirKiv=int(not GravirKiv)
    

    def escape():
            Grav.focus()

    def flipperA(*args):
            global a
            a=int(not a)
            
    def flipperB(*args):
        global b
        b=int(not b)
            
    def trigger(*args):
        if var==1:
            ManM._on_Label1_ButRel_1()
        else:
            ManM._on_Button2_Deact()
            
    def isready2(*args):
        if Mark.get()+Mark2.get()==2:
            MarkRdy.set(3)
        else:
            pass
       
            
    def markrdy2x(*args):
        if MarkRdy.get()==3:
            Mark.set(0)
            Mark2.set(0)
            C2x.focus()
        
            
    #Adjust sys.path so we can find other modules of this project
    import sys
    if '.' not in sys.path:
        sys.path.append('.')
    #Put lines to import other modules of this project here
     
    
    if __name__ == '__main__':

        Root = Tk()
        import Tkinter
        Tkinter.CallWrapper = rpErrorHandler.CallWrapper
        del Tkinter
        App = Intro1(Root)
        App.pack(expand='yes',fill='both')
        Grav=Grav1()
        ManM=ManMode()
        ManPass=ManualPassword()
        Manu1=Manual1()
        M2x=Man2x()
        M2=Man2()
        M1=Man1()
        InvPass=InvalidPassword()
        Err=Error()
        C2x=Confirm2x()
        C2=Confirm2()
        C1=Confirm1()
        


        Root.attributes('-fullscreen', True)
        Root.title('Intro1')  
        Root.bind('<Escape>',quit)
        #Vátozók outputra
        
        #Változók inputra
        BalInp=StringVar()
        JobbInp=StringVar()
        #Belső változók
        Mark=IntVar()
        Mark2=IntVar()
        Induktiv1=IntVar()
        Induktiv2=IntVar()
        
        Mark.trace('w',isready2)
        Mark2.trace('w',isready2)
        MarkRdy=IntVar()
        MarkRdy.trace('w',markrdy2x)
        print "Rooooooooooooooooooooooooooooooooooooooooooooooooot"
        
        
        #Beragadó billentyühöz
        
        #Pneumatika Manual mode
        flipPneu=IntVar()
        flipPneu.trace('r',flipPneum)
        
        #Vákum szivattyú Manual mode
        flipVacum=IntVar()
        flipVacum.trace('r',flipVacumm)
        
        #Kamera Manual mode
        flipKamera=IntVar()
        flipKamera.trace('r',flipKameram)
        
        #Gravírozó Manual mode
        flipGravir=IntVar()
        flipGravir.trace('r',flipGravirm)
        
        #Bal és jobb kiválaszt
        var=IntVar()
        FlipBal=StringVar()
        FlipJobb=StringVar()
        FlipBal.trace('r',flipperA)
        FlipJobb.trace('r',flipperB)
        var.trace('w',trigger)
       
  
        # controll
        
        
        def my_callback(channel):
            if rs485.readRDY==1 and rs232.WrtRDY==1: 
                #inp2.GetInp(bus.read_byte(0x38))
                inp.GetInp(bus.read_byte(PCF8574))
                print "falling edge detected on 21"  
  
        def my_callback2(channel):  
            
            if rs485.readRDY==1 and rs232.WrtRDY==1:
                print rs232.WrtRDY
                #inp.GetInp(bus.read_byte(PCF8574))
                inp2.GetInp(bus.read_byte(0x38))
                print "falling edege on 19"
   
    
        GPIO.add_event_detect(21, GPIO.FALLING, callback=my_callback, bouncetime=500)     
        GPIO.add_event_detect(19, GPIO.FALLING, callback=my_callback2, bouncetime=500)
        
        def quit(Event=None):
            sys.exit()
        Root.mainloop()
        
    #--------------------------------------------------------------------------#
    # User code should go above this comment.                                  #
    #--------------------------------------------------------------------------#
except:
    rpErrorHandler.RunError()