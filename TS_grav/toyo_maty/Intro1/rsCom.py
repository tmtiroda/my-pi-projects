#!/usr/bin/env python
import serial
import string

ser = serial.Serial(port='/dev/ttyRPC0', baudrate=9600,writeTimeout=1,timeout=3)
SR232=serial.Serial(port='/dev/ttyAMA0', baudrate=9600, writeTimeout=1)


class rscom485:
    readRDY=0
    MarkString1=''
    MarkString2=''
    ManMark1=''
    ManMark2=''
    
    MarkNoread=0
    Noread1=0
    Noread2=0
    ComunicationError=0
    readBuffer=[]
    
    def __init__(self,prefix='',sufix='',StringMod1='',StringMod2='',string_united='',readCount=0,Pos1=0,Pos2=0,readRD=0,String1='',String2=''):
        self.prefix=prefix
        self.sufix=sufix
        self.string_united=string_united
        self.StringMod1=StringMod1
        self.StringMod2=StringMod2
        self.readCount=readCount
        self.Pos1=Pos1
        self.Pos2=Pos2
        self.readRDY=readRD
        self.MarkString1=String1
        self.MarkString2=String2
        

        try:
            ser
        except:
            print('failed.')
            print('possible causes:')
            print('1) the raspicomm device driver is not loaded. type \'lsmod\' and verify that you \'raspicommrs485\' is loaded.')
            print('2) the raspicomm device driver is in use. Is another application using the device driver?')
            print('3) something went wrong when loading the device driver. type \'dmesg\' and check the kernel messages')
            exit()

        print('successful.')

    def rsread(self):
        self.Noread1=0
        self.Noread2=0
        self.MarkNoread=0
        self.ComunicationError=0
        # read in a loop
        print('start reading from the rs-485 port')
        val=0
        j=0
        
        while val == 0:
            
            if j<160:
                self.readBuffer.append(ser.read(1))
                
                if ser.timeout==3:
                    ser.timeout=0.01
                    
                j=j+1
            else:
                val=1
                ser.timeout=3
                                  
        #print the stuff out
        self.string_united='';
        self.string_united=self.string_united.join(self.readBuffer)
        print self.readBuffer
        print self.string_united
        
        try:
            a=self.string_united.index("%C%")
            b=self.string_united.rindex(";")
            self.StringMod1=self.string_united[a+3:b]
            print self.StringMod1
            self.ComunicationError=0
            if self.StringMod1=="NOREAD":
                self.MarkNoread=1
            
        except:
            print "Invalid Data 1"
            self.ComunicationError=1
        
        try:
            Po1=self.StringMod1.index(",")
            ps1=self.StringMod1.index("(")  
            self.Pos1=self.StringMod1[Po1-4:Po1]
            self.StringMod1=self.StringMod1[0:ps1-1]
            print "POS1", int(self.Pos1)
            
            if int(self.Pos1)<700:
                self.MarkString2=self.StringMod1
                print self.StringMod1
                self.Noread2=0
            
            if int(self.Pos1)>700:
                self.MarkString1=self.StringMod1
                print self.StringMod1
                self.Noread1=0
            else:
                print "pos 1 error"   
            
            
        except:
            print "read Error"

        
        try:  
            c=self.string_united.index(";")
            d=self.string_united.index("%D%")
            self.StringMod2=self.string_united[c+1:d]
            
            print self.StringMod2
            self.ComunicationError=0
            
        except:
            print self.StringMod2
            
        try:
            Po2=self.StringMod2.index(",")
            ps2=self.StringMod2.index("(")
            self.Pos2=self.StringMod2[Po2-4:Po2]
            self.StringMod2=self.StringMod2[0:ps2-1]
            print int(self.Pos2)
        except:
            if int(self.Pos1)<700:
                self.Noread2=1
                self.MarkString1="NOREAD"
                print "Noread 2 to"
            if int(self.Pos1)>700:
                self.Noread1=1
                self.MarkString2="NOREAD"
                print "Noread 1 to"
            
        if int(self.Pos2)<700 and self.StringMod2!="NOREAD":
            self.MarkString2=self.StringMod2
            print self.StringMod2  
            self.Noread2=0
            print "pos2", int(self.Pos2)
            print "es itt meg mi van?"
        if int(self.Pos2)>700 and self.StringMod2!="NOREAD":
            self.MarkString1=self.StringMod2
            print self.StringMod2 
            self.Noread1=0
            #print "pos2", int(self.Pos2)
            print "iitt mi a fasz van?"
        else:
            print "POS Error 2"
                
        print "Noread1",self.Noread1
        print "noread2",self.Noread2
        print "Mod1", self.MarkString1
        print "Mod2", self.MarkString2
       
            
        
        #self.MarkString2="M23314000148251"
        
                
        del self.readBuffer[:]
        self.readRDY=1

class rscom232:
    Wbal=0
    Wjobb=0
    WrtRDY=1
    StringMod2=''
    StringMod1=''
    
    def __init__(self,prefix='',sufix='',string_united='',readCount=0,MarkString1='',MarkString2=''):
        pass   
       
    def getStr(self,MarkString1,MarkString2):
    
        self.StringMod1=MarkString1

        self.StringMod2=MarkString2
    
    def rswrite(self,Noread1,Noread2,MarkNoread,ComunicationError):
        self.WrtRDY=0

        
        Strin=''
        
        if self.Wbal==0 and self.Wjobb==0:
            Strin="%A%"+"00"+"A&"+self.StringMod1+"&A"+"B&"+self.StringMod2+"&B"+"%B%"
        if self.Wbal==1 and Noread2==0 and MarkNoread==0 and ComunicationError==0 and self.Wjobb==0:
            Strin="%A%"+"10"+"A&"+self.StringMod1+"&A"+"%B%"
        if self.Wbal==1 and Noread1==0 and Noread2==0 and self.Wjobb==1 and MarkNoread==0 and ComunicationError==0:
            Strin="%A%"+"11"+"A&"+self.StringMod1+"&A"+"B&"+self.StringMod2+"&B"+"%B%"
        if self.Wjobb==1 and Noread1==0 and MarkNoread==0 and ComunicationError==0 and self.Wbal==0:
            Strin="%A%"+"01"+"B&"+self.StringMod2+"&B"+"%B%"
        else:
            print "No data to write"
        
        # open the port
        try:
            SR232
        except:
            print('failed.')
            print('possible causes:')
            print('1) the raspicomm device driver is not loaded. type \'lsmod\' and verify that you \'raspicommrs485\' is loaded.')
            print('2) the raspicomm device driver is in use. Is another application using the device driver?')
            print('3) something went wrong when loading the device driver. type \'dmesg\' and check the kernel messages')
            exit()
        

        print('successful.')
        # write
        print('start writing to the rs-232 port')
        SR232.write(Strin)
        SR232.flush()
        print Strin
        Strin=''
        ser.close
        self.WrtRDY=1

#adat=rscom232()
#adat.rsread()

#adat1=rscom485()
#adat1.rsread()
#adat=rscom232()
#adat.Wbal=1
#adat.Wjobb=1
#adat.rswrite()





        
            
