#!/usr/bin/env python

import smbus
import time
import string

# this lets us have a time delay (see line 12)  
import binascii
import string
import OPP


bus = smbus.SMBus(1) # Rev 2 Pi uses 1

class OPUT:
    
    OPa1=0
    OPa2=0
    OPa3=0
    OPa4=0
    OPa5=0
    OPa6=0
    OPa7=0
    OPa8=0
        
    
    OPb1=0
    OPb2=0
    OPb3=0
    OPb4=0
    OPb5=0
    OPb6=0
    OPb7=0
    OPb8=0
    
    
    OPtStringa=''
    
    OPtStringb=''
    
    OPtINTa=255
    
    OPtINTb=255

  
    def __init__(self,DEVICE=0x20,IODIRA=0x00,IODIRB=0x01,OLATA=0x14,OLATB=0x15):
        
        self.DEVICE = 0x20 # Device address (A0-A2)
        self.IODIRA = 0x00 # Pin direction register
        self.IODIRB = 0x01
        self.OLATA  = 0x14 # Register for outputs
        self.OLATB  = 0x15
        
         # Set output all 7 output bits to 0
        bus.write_byte_data(self.DEVICE,self.OLATA,255)
        bus.write_byte_data(self.DEVICE,self.OLATB,255)
        
        bus.write_byte_data(self.DEVICE,self.IODIRA,0x00)
        bus.write_byte_data(self.DEVICE,self.IODIRB,0x00)
        

    def Default(self):
         
        self.OPa1=0
        self.OPa2=0
        self.OPa3=0
        self.OPa4=0
        self.OPa5=0
        self.OPa6=0
        self.OPa7=0
        self.OPa8=0
            
        self.OPb1=0
        self.OPb2=0
        self.OPb3=0
        self.OPb4=0
        self.OPb5=0
        self.OPb6=0
        self.OPb7=0
        self.OPb8=0
        
        
        self.OPtStringa=''
    
        self.OPtStringb=''
        
        self.OPtINTa=255
        
        self.OPtINTb=255
        
   
    def Initialize(self,LOPa1,LOPa2,LOPa3,LOPa4,LOPa5,LOPa6,LOPa7,LOPa8,LOPb1,LOPb2,LOPb3,LOPb4,LOPb5,LOPb6,LOPb7,LOPb8):
        self.OPa1=LOPa
        self.Opa2=LOPa
        self.OPa3=LOPa
        self.OPa4=LOPa
        self.OPa5=LOPa
        self.OPa6=LOPa
        self.OPa7=LOPa
        self.OPa8=LOPa
            
        
        self.OPb1=LOPb
        self.Opb2=LOPb
        self.OPb3=LOPb
        self.OPb4=LOPb
        self.OPb5=LOPb
        self.OPb6=LOPb
        self.OPb7=LOPb
        self.OPb8=LOPb
        
        self.OPtStringa=''
        
        self.OPtStringb=''
        
        DatTa=[]
        DatTb=[]
        
        self.OPa1
        self.OPa2
        self.OPa3
        self.OPa4
        self.OPa5
        self.OPa6
        self.OPa7
        self.OPa8
        
        a1=int(not(OPa1))
        a2=int(not(OPa2))
        a3=int(not(OPa3))
        a4=int(not(OPa4))
        a5=int(not(OPa5))
        a6=int(not(OPa6))
        a7=int(not(OPa7))
        a8=int(not(OPa8))
        
        self.OPb1
        self.OPb2
        self.OPb3
        self.OPb4
        self.OPb5
        self.OPb6
        self.OPb7
        self.OPb8
        
        b1=int(not(OPb1))
        b2=int(not(OPb2))
        b3=int(not(OPb3))
        b4=int(not(OPb4))
        b5=int(not(OPb5))
        b6=int(not(OPb6))
        b7=int(not(OPb7))
        b8=int(not(OPb8))
        
        DatTa.append(str(a1))
        DatTa.append(str(a2))
        DatTa.append(str(a3))
        DatTa.append(str(a4))
        DatTa.append(str(a5))
        DatTa.append(str(a6))
        DatTa.append(str(a7))
        DatTa.append(str(a8))
        
        DatTb.append(str(b1))
        DatTb.append(str(b2))
        DatTb.append(str(b3))
        DatTb.append(str(b4))
        DatTb.append(str(b5))
        DatTb.append(str(b6))
        DatTb.append(str(b7))
        DatTb.append(str(b8))
        
        
        self.OPtStringa=''.join(DatTa)
        
        self.OPtStringb=''.join(DatTb)
        #print self.OPtStringa
        
        
        #print self.OPtStringb
        
        self.OPtINTa=int(self.OPtStringa,2)
        self.OPtINTb=int(self.OPtStringb,2)
        
        self.OPtStringa=''
        self.OPtStringb=''
    
    def Write(self):
        
        DatTa=[]
        DatTb=[]
        
        self.OPa1
        self.OPa2
        self.OPa3
        self.OPa4
        self.OPa5
        self.OPa6
        self.OPa7
        self.OPa8
        
        a1=int(not(self.OPa1))
        a2=int(not(self.OPa2))
        a3=int(not(self.OPa3))
        a4=int(not(self.OPa4))
        a5=int(not(self.OPa5))
        a6=int(not(self.OPa6))
        a7=int(not(self.OPa7))
        a8=int(not(self.OPa8))
        
        self.OPb1
        self.OPb2
        self.OPb3
        self.OPb4
        self.OPb5
        self.OPb6
        self.OPb7
        self.OPb8
        
        b1=int(not(self.OPb1))
        b2=int(not(self.OPb2))
        b3=int(not(self.OPb3))
        b4=int(not(self.OPb4))
        b5=int(not(self.OPb5))
        b6=int(not(self.OPb6))
        b7=int(not(self.OPb7))
        b8=int(not(self.OPb8))
        
        DatTa.append(str(a1))
        DatTa.append(str(a2))
        DatTa.append(str(a3))
        DatTa.append(str(a4))
        DatTa.append(str(a5))
        DatTa.append(str(a6))
        DatTa.append(str(a7))
        DatTa.append(str(a8))
        
        DatTb.append(str(b1))
        DatTb.append(str(b2))
        DatTb.append(str(b3))
        DatTb.append(str(b4))
        DatTb.append(str(b5))
        DatTb.append(str(b6))
        DatTb.append(str(b7))
        DatTb.append(str(b8))
        
        
        self.OPtStringa=''.join(DatTa)
        
        self.OPtStringb=''.join(DatTb)
        #print self.OPtStringa
        #print self.OPtStringb
        
        self.OPtINTa=int(self.OPtStringa,2)
        self.OPtINTb=int(self.OPtStringb,2)
        #print self.OPtINTa
        #print self.OPtINTb
        
        bus.write_byte_data(self.DEVICE,self.OLATA,self.OPtINTa)
        bus.write_byte_data(self.DEVICE,self.OLATB,self.OPtINTb)
        
    def disable(self):
        self.destroy()
        