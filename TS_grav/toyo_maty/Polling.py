#!/usr/bin/env python
import RPi.GPIO as GPIO  
  
GPIO.setmode(GPIO.BCM)
 

import smbus
import time
import binascii
import string
import OPP
import rsCom 
from rsCom import *

from threading import Timer

Oput=OPP.OPUT()

bus=smbus.SMBus(1)
PCF8574=0x39

rs485=rsCom.rscom485()
rs485.readRDY=1
rs232=rsCom.rscom232()

def Asztal():
    Oput.OPa4=1
    Oput.Write()
    print "asztal"
    inp.ControllBits=1
    inp.ControllBits2=1
    
def AsztalKi():
    Oput.OPa4=0
    Oput.Write()
    print "Asztalki"
    inp.ControllBits1=1
    
def Trig():
    print "Trigg"
    print inp.ControllBits
    if inp.ControllBits==1:
        Oput.OPb1=1
        Oput.Write()
        time.sleep(0.3)
        Oput.OPb1=0
        Oput.Write()
        print "TRIGGER"
        inp.ControllBits=0
        inp.Triggered=1
    
def ReadWt():
    if inp.Triggered==1: 
        rs485.readRDY=0
        rs485.rsread()
        j=0
        while rs485.readRDY==0 or j<100:
            time.sleep(0.05)
            j=j+1
        rs232.getStr(rs485.MarkString1,rs485.MarkString2)
        MarkEnable()
        rs232.rswrite(rs485.Noread1,rs485.Noread2,rs485.MarkNoread,rs485.ComunicationError)
        rs485.readRDY=1
        inp.Triggered=0
        
    
def Vakum():
    Oput.OPa5=1
    Oput.Write()
    
def Vakum0():
    Oput.OPa5=0
    Oput.Write()
    
def VakumKi():
    Oput.OPa5=0
    time.sleep(0.05)
    Oput.Write()
    Oput.OPa6=1
    Oput.Write()
    time.sleep(0.2)
    Oput.OPa6=0
    Oput.Write()
    
def VakumKi0():
    Oput.OPa6=0
    Oput.Write()

def VakumKiAuto():
    if inp.ControllBits1==1:
        Oput.OPa5=0
        time.sleep(0.05)
        Oput.Write()
        Oput.OPa6=1
        Oput.Write()
        time.sleep(0.2)
        Oput.OPa6=0
        Oput.Write()
        inp.ControllBits1=0

def Deff():
    Oput.Default()
    Oput.Write()
    inp2.ControllBits2=1
    print "Deffff"
    
    
def MarkEnable():
    Oput.OPb3=1
    Oput.Write()
    time.sleep(0.2)
    Oput.OPb3=0
    Oput.Write()
    
  
def OutputHandler(Elsoveg,Hatsoveg,Balgomb,Jobbgomb,ManualM,ElsoInd,HatsoInd,Veszkor):
    print "outputs"
    Eveg=Elsoveg
    Hveg=Hatsoveg
    Balg=Balgomb
    Jobbg=Jobbgomb
    ManM=ManualM
    Ind1=ElsoInd
    Ind2=HatsoInd
    Veszk=Veszkor
    
    print "ind1" ,Ind1
    print "ind2" ,Ind2
    
    rdy=0
    
    #automode eleje
    if Jobbg==1 and Balg==1 and ManM==0 and Veszk==0 and Hveg==0 and Eveg==1:
        Vakum()
        if inp2.Vakumk==1:
            Asztal()
            print "someth"
            #if Kival1==1 and Ind1==1 and Kival2==0: 
            #    Asztal()
            #if Kival2==1 and Ind2==1 and Kival1==0:
            #    Asztal()
            #if Kival1==1 and Kival2==1 and Ind1==1 and Ind2==1:
            #    Asztal()

    # automode vege       
    if Jobbg==1 and Balg==1 and ManM==0 and Veszk==0 and Hveg==1 and Eveg==0 and inp2.Markrdy==1 and inp2.Markerr==0 and inp2.Marking==1 and rs485.readRDY==1:
        AsztalKi()
        
    #manual asztal
    if Jobbg==1 and Balg==1 and ManM==1 and Veszk==0:
        VakumKi0()
        Vakum0()
        if Hveg==1:
            AsztalKi()
        if Eveg==1:
            Asztal()
            
    #automode trigger
    if Hveg==1 and ManM==0 and Veszk==0 and inp2.Markrdy==1:
        rs232.Wbal=0
        rs232.Wjobb=0
        Trig()
        ReadWt()
        
    
    if Jobbg==0 and ManM==0 and Veszk==0 and Eveg==0 and inp2.Markerr==1:
        print "rs232 Error"
    if Balg==1 and ManM==1 and Veszk==0 and Eveg==1:
        Vakum()
        
    #auto mode vege vakum kifuj
    if Jobbg==0 and ManM==0 and Veszk==0 and Eveg==1:
        VakumKiAuto()
        
    #alapallas
    if Balg==0 and ManM==1 and Veszk==0 and Eveg==1 and Jobbg==0:
        VakumKi0()
        Vakum0()
    if Jobbg==0 and ManM==1 and Veszk==0 and Eveg==1 and Balg==0:
        pass
    if Jobbg==1 and ManM==1 and Veszk==0 and Eveg==1:
        pass
    
    #vissza alap hejzetbe veszkor utana
    if Veszk==1:
        Deff()
        print "veszkor", Veszk
    if ManM==1 and Veszk==0:
        pass
        
    
def OutputHandler2(MarkErr,MarkPos,MarkinG,VakumOK,Inp5,Inp6,Inp7,Inp8):
    print "outputs2"
    MarkE=MarkErr
    MarkR=MarkPos
    Marking=MarkinG
    VacOK=VakumOK
    inP5=Inp5
    inP6=Inp6
    inP7=Inp7
    inP8=Inp8
    print "markred", MarkR
    print "markp", Marking
    print "markERR",MarkE
    
    print "VacOK", VacOK
    if VacOK==1:
        inp2.Vakumk=1  
    if VacOK==0:
        inp2.Vakumk=0
    if MarkE==1:
        inp2.Markerr=1
    if MarkE==0:
        inp2.Markerr=0
    if MarkR==1:
        inp2.Markrdy=1
    if MarkR==0:
        inp2.Markrdy=0
    if Marking==1:
        inp2.Marking=1
    if Marking==0:
        inp2.Marking=0
    
class innput1:
    
    Elsoveg=0
    Hatsoveg=0
    Balgomb=0
    Jobbgomb=0
    ManualM=0
    ElsoInd=0
    HatsoInd=0
    Veszkor=0
    RawInp=0
    
    ControllBits=0
    ControllBits1=0
    ControllBits2=0
    Triggered=0
    
    

    def __init__(self,RawInp='',PCF8574=0x39,in1=0,in2=0,in3=0,in4=0,in5=0,in6=0,in7=0,in8=0):
        self.InpData=''
            
        self.Inputs=[self.Elsoveg,self.Hatsoveg,self.Balgomb,self.Jobbgomb,self.ManualM,self.ElsoInd,self.HatsoInd,self.Veszkor]
            
    def GetInp(self,pcfdata):
            
        try:
            self.RawInp=pcfdata
            print self.RawInp
            self.InpData='{0:08b}'.format(self.RawInp)
            print "initiated"
            print self.InpData
            
        except:
            print    "Input_Mallfunction"
        
        print "get Inp function"
        tin1=self.InpData[0]
        tin2=self.InpData[1]
        tin3=self.InpData[2]
        tin4=self.InpData[3]
        tin5=self.InpData[4]
        tin6=self.InpData[5]
        tin7=self.InpData[6]
        tin8=self.InpData[7]
    
        #input value 
        self.in1=int(not(int(tin1)))
        self.in2=int(not(int(tin2)))
        self.in3=int(not(int(tin3)))
        self.in4=int(not(int(tin4)))
        self.in5=int(not(int(tin5)))
        self.in6=int(not(int(tin6)))
        self.in7=int(not(int(tin7)))
        self.in8=int(not(int(tin8)))
        
        #inputs by names
        self.Elsoveg=self.in1
        self.Hatsoveg=self.in2
        self.Balgomb=self.in3
        self.Jobbgomb=self.in4
        self.ManualM=self.in6
        self.ElsoInd=self.in7
        self.HatsoInd=self.in8
        self.Veszkor=self.in5
        
        self.InpData=''
        #input functions
        OutputHandler(self.Elsoveg,self.Hatsoveg,self.Balgomb,self.Jobbgomb,self.ManualM,self.ElsoInd,self.HatsoInd,self.Veszkor)

class innput2:
        
    MarkinG=0
    MarkPos=0
    MarkErr=0
    VakumOK=0
    Inp5=0
    Inp6=0
    Inp7=0
    Inp8=0
    
    ControllBits=0
    ControllBits1=0
    ControllBits2=0
    
    Vakumk=0
    Marking=0
    Markerr=0
    Markrdy=0
    
    def __init__(self,RawInp='',PCF8573=0x38,in1=0,in2=0,in3=0,in4=0,in5=0,in6=0,in7=0,in8=0):
        self.InpData=''
        
    def GetInp(self,pcfdata):
        
        try:
            self.RawInp=pcfdata
            print self.RawInp
            self.InpData='{0:08b}'.format(self.RawInp)
            print "initiated"
            print self.InpData
            
        except:
            print    "Input_Mallfunction"
        
        print "get Inp function"
        tin1=self.InpData[0]
        tin2=self.InpData[1]
        tin3=self.InpData[2]
        tin4=self.InpData[3]
        tin5=self.InpData[4]
        tin6=self.InpData[5]
        tin7=self.InpData[6]
        tin8=self.InpData[7]
    
        #input value 
        self.in1=int(not(int(tin1)))
        self.in2=int(not(int(tin2)))
        self.in3=int(not(int(tin3)))
        self.in4=int(not(int(tin4)))
        self.in5=int(not(int(tin5)))
        self.in6=int(not(int(tin6)))
        self.in7=int(not(int(tin7)))
        self.in8=int(not(int(tin8)))
        
        #inputs by names
        self.MarkErr=self.in1
        self.MarkPos=self.in2
        self.MarkinG=self.in3
        self.VakumOK=self.in4
        self.Inp5=self.in5
        self.Inp6=self.in6
        self.Inp7=self.in7
        self.Inp8=self.in8
    
        
        self.InpData=''
        #input functions
        
        
        OutputHandler2(self.MarkErr,self.MarkPos,self.MarkinG,self.VakumOK,self.Inp5,self.Inp6,self.Inp7,self.Inp8)
        
inp=innput1()
inp2=innput2()

'''
def baz():
    print "baz"
    inp.GetInp(bus.read_byte(PCF8574))
    time.sleep(0.1)
    inp2.GetInp(bus.read_byte(0x38))
    Poll()
    
def Poll():
    timer=Timer(0.5,baz)
    timer.start()
    
Poll()
'''
GPIO.setup(19, GPIO.IN, pull_up_down=GPIO.PUD_UP)  
GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_UP)
def my_callback(channel):
    
    if rs485.readRDY==1: 
        inp2.GetInp(bus.read_byte(0x38))
        inp.GetInp(bus.read_byte(PCF8574))
    print "falling edge detected on 19"  
  
def my_callback2(channel):  
    print "falling edge detected on 16"
    if rs485.readRDY==1: 
        inp.GetInp(bus.read_byte(PCF8574))
        inp2.GetInp(bus.read_byte(0x38))
   
 
GPIO.add_event_detect(16, GPIO.FALLING, callback=my_callback, bouncetime=500)     
GPIO.add_event_detect(19, GPIO.FALLING, callback=my_callback2, bouncetime=500) 
    
i=0
while True:
    i=i+1


    