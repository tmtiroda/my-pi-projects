import serial
import string

ser = serial.Serial(port='/dev/ttyRPC0', baudrate=9600,writeTimeout=1,timeout=3)
SR232=serial.Serial(port='/dev/ttyAMA0', baudrate=9600, writeTimeout=1)
MarkString1=''
MarkString2=''


class rscom232:
    
    def __init__(self,prefix='',sufix='',StringMod1='',StringMod2='',string_united='',readCount=0):
        self.prefix=prefix
        self.sufix=sufix
        self.string_united=string_united
        self.StringMod1=StringMod1
        self.StringMod2=StringMod2
        self.readCount=readCount
        self.readBuffer=[]
        

        try:
            ser
        except:
            print('failed.')
            print('possible causes:')
            print('1) the raspicomm device driver is not loaded. type \'lsmod\' and verify that you \'raspicommrs485\' is loaded.')
            print('2) the raspicomm device driver is in use. Is another application using the device driver?')
            print('3) something went wrong when loading the device driver. type \'dmesg\' and check the kernel messages')
            exit()

        print('successful.')

    def rsread(self):
        # read in a loop
        print('start reading from the rs-232 port')
        val=0
        j=0
        
        while val == 0:
            
            if j<50:
                self.readBuffer.append(ser.read(1))
                
                if ser.timeout==3:
                    ser.timeout=0.05
                    
                j=j+1
            else:
                val=1
                ser.timeout=5
                                  
        #print the stuff out
        self.string_united='';
        self.string_united=self.string_united.join(self.readBuffer)
        print self.readBuffer
        print self.string_united
        
        try:
            a=self.string_united.index("M")
            b=self.string_united.index(";")
            self.StrinMod1=self.string_united[a:b]
            global MarkString1
            MarkString1=self.StringMod1
            
            print self.StrinMod1
            
        except:
            print "Invalid Data 1"
        try:  
            c=self.string_united.rindex("1")
            d=self.string_united.rindex("M")
            self.StringMod2=self.string_united[d+1:c+1]
            global MarkString2
            MarkString2=self.StringMod2
            
            print self.StrinMod2
            
        except:
            print "invalid Data 2"
               
            
        
class Rs232:
    
    def __init__(self,prefix='',sufix='',StringMod1='',StringMod2='',string_united='',readCount=0):
        
        global MarkString1
        global MarkString2
        
        self.StringMod1=MarkString1

        self.StringMod2=MarkString2
    
    def RsWrite(self):
        
        Strin="%A%M32914035848251%B%"
        
        if self.StringMod1=='':
            print "Empty String1 Rs232"
        else:
            Strin=self.StringMod1+'%'

        if self.StringMod2=='':
            print "Empty String2 Rs232"
        else:
            Strin=self.StringMod1+'%'
            
            
        

        print('this sample application writes to the rs-232 port')

        # open the port
        print('opening device /dev/ttyAMA0')
        try:
            SR232
        except:
            print('failed.')
            print('possible causes:')
            print('1) the raspicomm device driver is not loaded. type \'lsmod\' and verify that you \'raspicommrs485\' is loaded.')
            print('2) the raspicomm device driver is in use. Is another application using the device driver?')
            print('3) something went wrong when loading the device driver. type \'dmesg\' and check the kernel messages')
            exit()

        print('successful.')
        # write
        print('start writing to the rs-232 port')
        SR232.write(Strin)
        ser.close

#adat=rscom232()
#adat.rsread()
adat=Rs232()
adat.RsWrite()
