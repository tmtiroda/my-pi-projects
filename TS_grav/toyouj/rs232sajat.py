import serial
import string

ser = serial.Serial(port='/dev/ttyRPC0', baudrate=9600,writeTimeout=1,timeout=3)
SR232=serial.Serial(port='/dev/ttyAMA0', baudrate=9600, writeTimeout=1)
MarkString1=''
MarkString2=''
MarkNoread=0
Noread1=0
Noread2=0
ComunicationError=0



class rscom485:
    
    def __init__(self,prefix='',sufix='',StringMod1='',StringMod2='',string_united='',readCount=0,Pos1=0,Pos2=0):
        self.prefix=prefix
        self.sufix=sufix
        self.string_united=string_united
        self.StringMod1=StringMod1
        self.StringMod2=StringMod2
        self.readCount=readCount
        self.readBuffer=[]
        self.Pos1=Pos1
        self.Pos2=Pos2
        

        try:
            ser
        except:
            print('failed.')
            print('possible causes:')
            print('1) the raspicomm device driver is not loaded. type \'lsmod\' and verify that you \'raspicommrs485\' is loaded.')
            print('2) the raspicomm device driver is in use. Is another application using the device driver?')
            print('3) something went wrong when loading the device driver. type \'dmesg\' and check the kernel messages')
            exit()

        print('successful.')

    def rsread(self):
        # read in a loop
        print('start reading from the rs-485 port')
        val=0
        j=0
        
        while val == 0:
            
            if j<160:
                self.readBuffer.append(ser.read(1))
                
                if ser.timeout==3:
                    ser.timeout=0.01
                    
                j=j+1
            else:
                val=1
                ser.timeout=3
                                  
        #print the stuff out
        self.string_united='';
        self.string_united=self.string_united.join(self.readBuffer)
        print self.readBuffer
        print self.string_united
        
        try:
            a=self.string_united.index("%C%")
            b=self.string_united.rindex(";")
            self.StringMod1=self.string_united[a+3:b]
            print self.StringMod1
            
            if self.StringMod1=="NOREAD":
                global MarkNoread
                MarkNoread=1
            
        except:
            print "Invalid Data 1"
            global ComunicationError
            ComunicationError=1
        
        try:
            Po1=self.StringMod1.index("(")
           
            self.Pos1=self.StringMod1[Po1+1:Po1+5]
            self.StringMod1=self.StringMod1[0:Po1-1]
            print int(self.Pos1)
            
            
            if int(self.Pos1)<700:
                global MarkString1
                MarkString1=self.StringMod1
                print self.StringMod1
                
            elif int(self.Pos1)>700:
                global MarkString2 
                MarkString2=self.StringMod1
                print self.StringMod1
            else:
                print "POS Error"    
            
        except:
            print "read Error"

        
        try:  
            c=self.string_united.rindex(";")
            d=self.string_united.rindex("%D%")
            self.StringMod2=self.string_united[c+1:d]
            
            print self.StringMod2
            
        except:
            print "invalid Data 2"
            ComunicationError=1
            
        try:
            Po2=self.StringMod2.index("(")
           
            self.Pos2=self.StringMod2[Po2+1:Po2+5]
            self.StringMod2=self.StringMod2[0:Po2-1]
            print self.Pos2
            
            if int(self.Pos2)<700:
                MarkString1=self.StringMod2
                print self.StrinMod2      
            elif int(self.Pos2)>700:
                MarkString2=self.StringMod2
                print self.StringMod2    
            else:
                print "POS Error"
        except:
            global Noread2
            global Noread1
            if int(self.Pos1)<700:
                Noread2=1
                print "Noread 1"
            if int(self.Pos1)>700:
                Noread1=1
                print "Noread 2"
                
        self.ReadBuffer=[]

class Rs232:
    Wbal=0
    Wjobb=0
    
    def __init__(self,prefix='',sufix='',StringMod1='',StringMod2='',string_united='',readCount=0):
        
        global MarkString1
        global MarkString2
        
        self.StringMod1=MarkString1

        self.StringMod2=MarkString2
        
    
    def rsWrite(self):

        
        Strin=''
        
        global Noread1
        global Noread2
        global MarkNoread
        global ComunicationError
        
        if self.Wbal==0 and self.Wjobb==0:
            Strin="%A%"+"00"+"A&"+self.StringMod1+"&A"+"B&"+self.StringMod2+"&B"+"%B%"
        elif self.Wbal==1 and Noread1==0 and self.Wjobb==0 and MarkNoread==0 and ComunicationError==0:
            Strin="%A%"+"10"+"A&"+self.StringMod1+"&A"+"%B%"
        elif self.Wbal==1 and Noread1==0 and Noread2==0 and self.Wjobb==1 and MarkNoread==0 and ComunicationError==0:
            Strin="%A%"+"11"+"A&"+self.StringMod1+"&A"+"B&"+self.StringMod2+"&B"+"%B%"
        elif self.Wbal==0 and self.Wjobb==1 and Noread2==0 and MarkNoread==0 and ComunicationError==0:
            Strin="%A%"+"01"+"B&"+self.StringMod2+"&B"+"%B%"
        else:
            print "No data to write"
        
        # open the port
        try:
            SR232
        except:
            print('failed.')
            print('possible causes:')
            print('1) the raspicomm device driver is not loaded. type \'lsmod\' and verify that you \'raspicommrs485\' is loaded.')
            print('2) the raspicomm device driver is in use. Is another application using the device driver?')
            print('3) something went wrong when loading the device driver. type \'dmesg\' and check the kernel messages')
            exit()

        print('successful.')
        # write
        print('start writing to the rs-232 port')
        SR232.write(Strin)
        print Strin
        Strin=''
        ser.close

#adat=rscom232()
#adat.rsread()

adat1=rscom485()
adat1.rsread()
adat=Rs232()
adat.Wbal=1
adat.Wjobb=1
adat.rsWrite()


print 'Markstr1' ,MarkString1
print 'Markstr2' ,MarkString2
print 'marknoread' ,MarkNoread
print 'Comerr' ,ComunicationError
print 'Noread1', Noread1
print 'Noread2', Noread2



        
            
