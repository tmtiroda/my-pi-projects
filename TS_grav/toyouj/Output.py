import smbus
import time

bus = smbus.SMBus(1) # Rev 2 Pi uses 1

A=0
B=0
 
DEVICE = 0x20 # Device address (A0-A2)
IODIRA = 0x00 # Pin direction register
IODIRB = 0x01
OLATA  = 0x14 # Register for outputs
OLATB  = 0x15

class Opmod:
    def __init__(self,OPa=255,OPb=255):
        # Set all GPA pins as outputs by setting
        # all bits of IODIRA register to 0
        
        # Set output all 7 output bits to 0
        bus.write_byte_data(DEVICE,OLATA,255)
        bus.write_byte_data(DEVICE,OLATB,255)
        
        bus.write_byte_data(DEVICE,IODIRA,0x00)
        bus.write_byte_data(DEVICE,IODIRB,0x00)
         
        # Set output all 7 output bits to 0
        bus.write_byte_data(DEVICE,OLATA,255)
        bus.write_byte_data(DEVICE,OLATB,255)
        
    def write(self,OPa,OPb):
        
        bus.write_byte_data(DEVICE,OLATA,OPa)
        bus.write_byte_data(DEVICE,OLATB,OPb)
        
    def disable(self):
        self.destroy()
        
Output=Opmod()

while True:

    
    Asztal = int(raw_input('Asztal be ? 1/0 '))
    Kamera = int(raw_input('kamera be ? 1/0 '))
    
    if Asztal == 1 and Kamera ==1:
        Output.write(239,191)
        time.sleep(0.1)
        Output.write(239,255)
        
    elif Asztal ==0 and Kamera ==1:
        Output.write(239,191)
        time.sleep(0.1)
        Output.write(239,255)
        
        
    elif Asztal ==1 and Kamera ==0:
        Output.write(239,255)
    else:
        Output.write(255,255)
    
    
    

    

