#!/usr/bin/python
#coding=utf8

import rpErrorHandler
from Tkinter import *
#------------------------------------------------------------------------------#
#                                                                              #
#                                     test                                     #
#                                                                              #
#------------------------------------------------------------------------------#
class test(Frame):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Frame.__init__,(self,Master),kw)
        self._Button1 = Button(self)
        self._Button1.pack(side='left')
        self._Button1.bind('<Any-ButtonPress>',self._on_Button1_Button__N)
        #
        #Your code here
        #
    #
    #Start of event handler methods
    #


    def _on_Button1_Button__N(self,Event=None):
        sys.exit()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---

try:
    #--------------------------------------------------------------------------#
    # User code should go after this comment so it is inside the "try".        #
    #     This allows rpErrorHandler to gain control on an error so it         #
    #     can properly display a Rapyd-aware error message.                    #
    #--------------------------------------------------------------------------#

    #Adjust sys.path so we can find other modules of this project
    import sys
    if '.' not in sys.path:
        sys.path.append('.')
    #Put lines to import other modules of this project here
    
    if __name__ == '__main__':

        Root = Tk()
        import Tkinter
        Tkinter.CallWrapper = rpErrorHandler.CallWrapper
        del Tkinter
        App = test(Root)
        App.pack(expand='yes',fill='both')

        Root.geometry('320x240+0+0')
        Root.title('test')
        Root.mainloop()
    #--------------------------------------------------------------------------#
    # User code should go above this comment.                                  #
    #--------------------------------------------------------------------------#
except:
    rpErrorHandler.RunError()