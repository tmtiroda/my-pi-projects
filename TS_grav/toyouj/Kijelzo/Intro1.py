#!/usr/bin/python
#coding=utf8



import rpErrorHandler
from Tkinter import *
#------------------------------------------------------------------------------#
#                                                                              #
#                                  ChangePass                                  #
#                                                                              #
#------------------------------------------------------------------------------#
class ChangePass(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_ChangePass_Key_Escape)
        self.Password = StringVar()
        self.Password1 = StringVar()
        self.NewPass = StringVar()
        self._Frame5 = Frame(self)
        self._Frame5.pack(side='top')
        self._Label6 = Label(self._Frame5
            ,text='Please enter password \n Kérem írja be a jelszavat')
        self._Label6.pack(side='left')
        self._Frame2 = Frame(self)
        self._Frame2.pack(side='top')
        self._Label1 = Label(self._Frame2,text='Old Password \n Régi jelszó')
        self._Label1.pack(side='left')
        self._Entry1 = Entry(self._Frame2,show='X',textvariable=self.Password)
        self._Entry1.pack(side='left')
        self._Entry1.bind('<KeyPress-Return>',self._on_Entry1_Key_Return)
        self._Frame1 = Frame(self)
        self._Frame1.pack(side='top')
        self._Label5 = Label(self._Frame1
            ,text='Please enter new password \n Kérem irja be az új jelszavat')
        self._Label5.pack(side='left')
        self._Frame4 = Frame(self)
        self._Frame4.pack(side='top')
        self._Label2 = Label(self._Frame4,anchor='w'
            ,text='New password \n Új jelszó')
        self._Label2.pack(side='left')
        self._Entry2 = Entry(self._Frame4,show='X',textvariable=self.Password1)
        self._Entry2.pack(anchor='e',side='left')
        self._Entry2.bind('<KeyPress-Return>',self._on_Entry2_Key_Return)
        self._Frame6 = Frame(self)
        self._Frame6.pack(side='top')
        self._Label4 = Label(self._Frame6
            ,text='Please confirm new password \n Kérem hagyja jóvá az új jelszavat')
        self._Label4.pack(side='left')
        self._Frame3 = Frame(self)
        self._Frame3.pack(side='top')
        self._Label3 = Label(self._Frame3
            ,text='New Password confirm \n Új jelszó jóváhagyás')
        self._Label3.pack(side='left')
        self._Entry3 = Entry(self._Frame3,textvariable=self.NewPass)
        self._Entry3.pack(side='left')
        self._Entry3.bind('<KeyPress-Return>',self._on_Entry3_Key_Return)
        #
        #Your code here
        #
        self._Entry1.focus()
    #
    #Start of event handler methods
    #


    def _on_Entry1_Key_Return(self,Event=None):
        Pass=self.Password.get()
        if Pass==Password:
            self._Entry2.focus()
        else:
            InvalidPassword().focus()
            self.destroy()
        pass

    def _on_Entry2_Key_Return(self,Event=None):
        Pass=self.Password1.get()
        if Pass==Password:
            self._Entry3.focus()
        else:
            InvalidPassword().focus()
            self.destroy()

    def _on_Entry3_Key_Return(self,Event=None):
        Password=self.NewPass.get()
        ManualPassword().focus()
        self.destroy()

    def on_ChangePass_Key_Escape(self,Event=None):
        Manual1().focus()
        self.destroy()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                   Confirm1                                   #
#                                                                              #
#------------------------------------------------------------------------------#
class Confirm1(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        kw['background'] = '#ff8000'
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_Confirm1_Key_Escape)
        self.bind('<KeyPress-Return>',self.on_Confirm1_Key_Return)
        self.baloldal = StringVar()
        self._Frame3 = Frame(self)
        self._Frame3.pack(expand='yes',fill='both',side='top')
        self._Label1 = Label(self._Frame3,activebackground='#ff8000'
            ,background='#ff8000',font=('Times New Roman', 15, '')
            ,highlightbackground='#ff8000',text='Confirmation !')
        self._Label1.pack(expand='yes',fill='both',side='top')
        self._Label2 = Label(self._Frame3,activebackground='#ff8000'
            ,background='#ff8000',font=('Times New Roman CE', 12, '')
            ,highlightbackground='#ff8000',text='Ellenörzés!')
        self._Label2.pack(expand='yes',fill='both',side='top')
        self._Frame2 = Frame(self,background='#8080ff')
        self._Frame2.pack(expand='yes',fill='both',side='top')
        self._Label9 = Label(self._Frame2,activebackground='#8080ff'
            ,background='#8080ff')
        self._Label9.pack(pady='7',side='top')
        self._Label3 = Label(self._Frame2,activebackground='#ffffff'
            ,background='#ffffff',font=('Times New Roman CYR', 20, '')
            ,foreground='#ff0000',highlightbackground='#ffffff',relief='groove'
            ,textvariable=self.baloldal)
        self._Label3.pack(expand='yes',fill='both',ipady='10',side='top')
        self._Label8 = Label(self._Frame2,activebackground='#8080ff'
            ,background='#8080ff')
        self._Label8.pack(pady='7',side='bottom')
        self._Frame1 = Frame(self)
        self._Frame1.pack(padx='5',side='top')
        self._Frame4 = Frame(self._Frame1,background='#ff8000')
        self._Frame4.pack(expand='yes',fill='both',side='left')
        self._Label4 = Label(self._Frame4,activebackground='#8080ff',anchor='w'
            ,background='#ff8000',font=('Times New Roman Baltic', 7, '')
            ,highlightbackground='#8080ff',text='Press <esc> to modify')
        self._Label4.pack(expand='yes',fill='both',side='top')
        self._Label5 = Label(self._Frame4,activebackground='#8080ff',anchor='w'
            ,background='#ff8000',font=('Times New Roman CE', 7, '')
            ,highlightbackground='#8080ff',text='Nyomjon <esc> -et a javításhoz')
        self._Label5.pack(expand='yes',fill='both',side='top')
        self._Frame6 = Frame(self._Frame1,background='#ff8000')
        self._Frame6.pack(padx='2',side='left')
        self._Frame5 = Frame(self._Frame1,background='#ff8000')
        self._Frame5.pack(expand='yes',fill='both',side='left')
        self._Label6 = Label(self._Frame5,activebackground='#8080ff',anchor='e'
            ,background='#ff8000',font=('Times New Roman', 7, '')
            ,highlightbackground='#8080ff',text='Press <Enter> to acknowledge')
        self._Label6.pack(expand='yes',fill='both',side='top')
        self._Label7 = Label(self._Frame5,activebackground='#8080ff',anchor='e'
            ,background='#ff8000',font=('Times New Roman CE', 7, '')
            ,highlightbackground='#8080ff'
            ,text='Nyomjon <Enter> -t a jóváhagyáshoz')
        self._Label7.pack(side='top')
        #
        self.baloldal.set(baloldal.get())
        
        #Your code here
        #
        self.attributes('-fullscreen', True)
    #
    #Start of event handler methods
    #
    #Start of event handler methods
    #


    def on_Confirm1_Key_Escape(self,Event=None):
        Man1().focus()
        self.destroy()

    def on_Confirm1_Key_Return(self,Event=None):
        BalInp.set(baloldal.get())
        self.after(100,Man1().focus())
        self.destroy()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                   Confirm2                                   #
#                                                                              #
#------------------------------------------------------------------------------#
class Confirm2(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_Confirm2_Key_Escape)
        self.bind('<KeyPress-Return>',self.on_Confirm2_Key_Return)
        self.jobboldal = StringVar()
        self._Frame7 = Frame(self)
        self._Frame7.pack(expand='yes',fill='both',side='top')
        self._Label1 = Label(self._Frame7,activebackground='#ff8000'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#ff8000',text='Confirmation !')
        self._Label1.pack(side='top')
        self._Label8 = Label(self._Frame7,activebackground='#ff8000'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#ff8000',text='Ellenörzés!')
        self._Label8.pack(side='top')
        self._Frame1 = Frame(self)
        self._Frame1.pack(side='top')
        self._Label7 = Label(self._Frame1,activebackground='#ffffff'
            ,background='#ffffff',font=('Times New Roman', 40, '')
            ,foreground='#ff0000',highlightbackground='#ffffff',relief='groove'
            ,textvariable=self.jobboldal)
        self._Label7.pack(expand='yes',fill='both',ipadx='320',pady='20'
            ,side='left')
        self._Frame8 = Frame(self,background='#ffffff')
        self._Frame8.pack(expand='yes',fill='both',side='top')
        self._Frame9 = Frame(self._Frame8)
        self._Frame9.pack(expand='yes',fill='both',side='left')
        self._Label3 = Label(self._Frame9,activebackground='#8080ff',anchor='w'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#8080ff',text='Press <esc> to modify')
        self._Label3.pack(padx='20',pady='20',side='top')
        self._Label5 = Label(self._Frame9,activebackground='#8080ff',anchor='w'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#8080ff',text='Nyomjon <esc> -et a javításhoz')
        self._Label5.pack(padx='20',pady='20',side='top')
        self._Frame5 = Frame(self._Frame8)
        self._Frame5.pack(anchor='e',expand='yes',fill='both',side='left')
        self._Label4 = Label(self._Frame5,activebackground='#8080ff',anchor='e'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#8080ff',text='Press <Enter> to acknowledge')
        self._Label4.pack(padx='20',pady='20',side='top')
        self._Label6 = Label(self._Frame5,activebackground='#8080ff',anchor='e'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#8080ff'
            ,text='Nyomjon <Enter> -t a jóváhagyáshoz')
        self._Label6.pack(padx='20',pady='20',side='top')
        #
        self.jobboldal.set(jobboldal.get())
        #Your code here
        #
        
        self.attributes('-fullscreen', True)
    #
    #Start of event handler methods
    #
    #Start of event handler methods
    #


    def on_Confirm2_Key_Escape(self,Event=None):
        Man2().focus()
        self.destroy()

    def on_Confirm2_Key_Return(self,Event=None):
        JobbInp.set(jobboldal.get())
        self.after(100,Man2().focus())
        self.destroy()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                  Confirm2x                                   #
#                                                                              #
#------------------------------------------------------------------------------#
class Confirm2x(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_Confirm2x_Key_Escape)
        self.baloldal = StringVar()
        self.jobboldal = StringVar()
        self._Frame2 = Frame(self)
        self._Frame2.pack(side='top')
        self._Label1 = Label(self._Frame2,activebackground='#ff8000'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#ff8000',text='Confirmation !')
        self._Label1.pack(side='top')
        self._Label2 = Label(self._Frame2,activebackground='#ff8000'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#ff8000',text='Ellenörzés!')
        self._Label2.pack(side='top')
        self._Frame1 = Frame(self)
        self._Frame1.pack(side='top')
        self._Label8 = Label(self._Frame1,activebackground='#ffffff'
            ,background='#ffffff',font=('Times New Roman', 40, '')
            ,foreground='#ff0000',highlightbackground='#ffffff',relief='groove'
            ,textvariable=self.jobboldal)
        self._Label8.pack(expand='yes',fill='both',ipadx='300',pady='20'
            ,side='top')
        self._Label7 = Label(self._Frame1,activebackground='#ffffff'
            ,background='#ffffff',font=('Times New Roman', 40, '')
            ,foreground='#ff0000',highlightbackground='#ffffff',relief='groove'
            ,textvariable=self.baloldal)
        self._Label7.pack(expand='yes',fill='both',ipadx='300',pady='20'
            ,side='top')
        self._Frame3 = Frame(self)
        self._Frame3.pack(side='top')
        self._Frame4 = Frame(self._Frame3)
        self._Frame4.pack(expand='yes',fill='both',side='left')
        self._Label3 = Label(self._Frame4,activebackground='#8080ff',anchor='w'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#8080ff',text='Press <esc> to modify')
        self._Label3.pack(padx='20',pady='20',side='top')
        self._Label4 = Label(self._Frame4,activebackground='#8080ff',anchor='w'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#8080ff',text='Nyomjon <esc> -et a javításhoz')
        self._Label4.pack(padx='20',pady='20',side='top')
        self._Frame5 = Frame(self._Frame3)
        self._Frame5.pack(expand='yes',fill='both',side='left')
        self._Label5 = Label(self._Frame5,activebackground='#8080ff',anchor='e'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#8080ff',text='Press <Enter> to acknowledge')
        self._Label5.pack(padx='20',pady='20',side='top')
        self._Label6 = Label(self._Frame5,activebackground='#8080ff',anchor='e'
            ,background='#ff8000',font=('Times New Roman', 18, '')
            ,highlightbackground='#8080ff'
            ,text='Nyomjon <Enter> -t a jóváhagyáshoz')
        self._Label6.pack(padx='20',pady='20',side='top')
        #
        #Your code here
        #
        self.jobboldal.set(jobboldal.get())
        self.baloldal.set(baloldal.get())
    #
    #Start of event handler methods
    #


    def on_Confirm2x_Key_Escape(self,Event=None):
        Man2x().focus()
        self.destroy()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                    Error                                     #
#                                                                              #
#------------------------------------------------------------------------------#
class Error(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)

        #
        #Your code here
        #
    #
    #Start of event handler methods
    #

    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                    Grav1                                     #
#                                                                              #
#------------------------------------------------------------------------------#
class Grav1(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #
        
        import time
        

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_Grav1_Key_Escape)
        self._Frame3 = Frame(self)
        self._Frame3.pack(expand='yes',fill='both',side='top')
        self._Label1 = Label(self._Frame3,background='#c0c0c0',bitmap='@bal.XBM'
            ,relief='raised')
        self._Label1.pack(expand='yes',fill='both',side='top')
        self._Label4 = Label(self._Frame3,background='#c0c0c0'
            ,bitmap='@jobb.XBM',relief='raised')
        self._Label4.pack(expand='yes',fill='both',side='top')
        self._Label4.bind('<Any-ButtonPress>',self._on_Label3_Button__N)
        self._Label4.bind('<ButtonRelease-1>',self._on_Label3_ButRel_1)
        self._Frame2 = Frame(self,background='#0080c0')
        self._Frame2.pack(expand='yes',fill='both',side='top')
        self._Label2 = Label(self._Frame2,background='#8080ff'
            ,font=('Times New Roman', 12, '')
            ,text='Work Select \n Munkadarab kiválasztás')
        self._Label2.pack(expand='yes',fill='both',side='left')
        self._Label2.bind('<ButtonRelease-1>',self._on_Label2_ButRel_1)
        self._Frame1 = Frame(self)
        self._Frame1.pack(expand='yes',fill='both',side='top')
        self._Button1 = Button(self._Frame1,command=self._on__Button1_command)
        self._Button1.pack(expand='yes',fill='both',side='left')
        #
        #Your code here
        #
        
        self.geometry("800x480+0+0")
        #self.attributes('-fullscreen', True)
       
        if a==1:
            self._Label1.configure(relief='sunken')
            self._Label1.configure(background='#00ff00')
            
        else :
            self._Label1.configure(relief='raised')
            self._Label1.configure(background='#c0c0c0')
        
        if b==1:
           self._Label4.configure(relief='sunken')
           self._Label4.configure(background='#00ff00')
        elif b==0:
          self._Label4.configure(relief='raised')
          self._Label4.configure(background='#c0c0c0')
        else:
           pass

        
    #
    #Start of event handler methods
    #


    def _on_Label2_ButRel_1(self,Event=None):
        ManMode().focus()
        self.after(200,self.destroy())

    def _on_Label3_ButRel_1(self,Event=None):
        pass

    def _on_Label3_Button__N(self,Event=None):
        pass

    def _on__Button1_command(self,Event=None):
        ManualPassword().focus()
        self.after(200,self.destroy())

    def on_Grav1_Key_Escape(self,Event=None):
        sys.exit()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                    Intro1                                    #
#                                                                              #
#------------------------------------------------------------------------------#
class Intro1(Frame):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #
        

        apply(Frame.__init__,(self,Master),kw)
        self._Label1 = Label(self,bitmap='@tmtlogo.XBM')
        self._Label1.pack(expand='yes',fill='both',side='top')
        #
     
        #Your code here
        #

            
        self.after(1300, lambda: Grav1().lift())
        self.after(1400, lambda: self.destroy())
        
        
        #self.after(1000, lambda: Fcuser().deiconify())
        
    

       

 
    #
    #Start of event handler methods
    #

    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                               InvalidPassword                                #
#                                                                              #
#------------------------------------------------------------------------------#
class InvalidPassword(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_InvalidPassword_Key_Escape)
        self._Button1 = Button(self,bitmap='warning',text='Invalid Password')
        self._Button1.pack(side='left')
        self._Button1.bind('<Any-ButtonPress>',self._on_Button1_Button__N)
        #
        #Your code here
        #
    #
    #Start of event handler methods
    #


    def _on_Button1_Button__N(self,Event=None):
        ManualPassword().lift()

    def on_InvalidPassword_Key_Escape(self,Event=None):
        escape()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                     Man1                                     #
#                                                                              #
#------------------------------------------------------------------------------#
class Man1(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.Keyb = StringVar()
        self._Frame2 = Frame(self)
        self._Frame2.pack(side='left')
        self._Button1 = Button(self._Frame2,bitmap='@balra.XBM'
            ,command=self._on__Button1_command)
        self._Button1.pack(side='left')
        self._Frame1 = Frame(self)
        self._Frame1.pack(side='left')
        self._Label1 = Label(self._Frame1,text='Manual Input (Right)')
        self._Label1.pack(side='top')
        self._Label2 = Label(self._Frame1,text='Kézi bevitel (jobb)')
        self._Label2.pack(side='top')
        self._Label3 = Label(self._Frame1,bitmap='@bal.XBM')
        self._Label3.pack(side='top')
        self._Label4 = Label(self._Frame1,font=('Times New Roman', 22, '')
            ,foreground='#ff0000',textvariable=self.Keyb)
        self._Label4.pack(side='top')
        self._Entry1 = Entry(self._Frame1,font=('Times New Roman', 21, '')
            ,takefocus=1,textvariable=self.Keyb)
        self._Entry1.pack(side='bottom')
        self._Entry1.bind('<KeyPress-Return>',self._on_Entry1_Key_Return)
        #
        #Your code here
        #
        self._Entry1.focus_set()
        self.geometry("800x480+0+0")
        #self.attributes('-fullscreen', True)
    #
    #Start of event handler methods
    #


    def _on_Entry1_Key_Return(self,Event=None):
        baloldal.set(self.Keyb.get())
       
        self._Entry1.delete(0, END)
        self._Entry1.insert(0, "")
        Confirm1().focus()
        self.after(200,self.destroy())

    def _on__Button1_command(self,Event=None):
        ManMode().focus()
        self.after(200,self.destroy())
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                     Man2                                     #
#                                                                              #
#------------------------------------------------------------------------------#
class Man2(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.Keyb = StringVar()
        self._Frame2 = Frame(self)
        self._Frame2.pack(side='left')
        self._Button1 = Button(self._Frame2,bitmap='@balra.XBM'
            ,command=self._on__Button1_command)
        self._Button1.pack(side='right')
        self._Frame1 = Frame(self)
        self._Frame1.pack(side='left')
        self._Label3 = Label(self._Frame1,text='Manual Input (Right)')
        self._Label3.pack(side='top')
        self._Label4 = Label(self._Frame1,text='Kézi bevitel (jobb)')
        self._Label4.pack(side='top')
        self._Label2 = Label(self._Frame1,bitmap='@jobb.XBM')
        self._Label2.pack(side='top')
        self._Label1 = Label(self._Frame1,font=('Times New Roman', 22, '')
            ,foreground='#ff0000',textvariable=self.Keyb)
        self._Label1.pack(fill='both',side='top')
        self._Entry1 = Entry(self._Frame1,font=('Times New Roman', 22, '')
            ,justify='center',takefocus=1,textvariable=self.Keyb)
        self._Entry1.pack(side='top')
        self._Entry1.bind('<KeyPress-Return>',self._on_Entry1_Key_Return)

        #Your code here
        #
        self._Entry1.focus_set()
        self.geometry("800x480+0+0")
        #self.attributes('-fullscreen', True)
        
        
        
        
        
    #
    #Start of event handler methods
    #


    def _on_Entry1_Key_Return(self,Event=None):
        jobboldal.set(self.Keyb.get())
       
        self._Entry1.delete(0, END)
        self._Entry1.insert(0, "")
        Confirm2().focus()
        self.after(200,self.destroy())

    def _on__Button1_command(self,Event=None):
        ManMode().focus()
        self.after(200,self.destroy())
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                    Man2x                                     #
#                                                                              #
#------------------------------------------------------------------------------#
class Man2x(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #
  

        apply(Toplevel.__init__,(self,Master),kw)
        self.Echo = StringVar()
        self.Echo2 = StringVar()
        self.EchoCont1 = StringVar()
        self.EchoCont2 = StringVar()
        self._Frame6 = Frame(self)
        self._Frame6.pack(side='left')
        self._Button1 = Button(self._Frame6,bitmap='@balra.XBM'
            ,command=self._on__Button1_command)
        self._Button1.pack(anchor='w',side='left')
        self._Frame5 = Frame(self)
        self._Frame5.pack(side='left')
        self._Frame3 = Frame(self._Frame5)
        self._Frame3.pack(side='top')
        self._Label1 = Label(self._Frame3,font=('Times New Roman', 15, '')
            ,text='Baloldal')
        self._Label1.pack(expand='yes',fill='both',ipadx='100',side='top')
        self._Entry1 = Entry(self._Frame3,textvariable=self.Echo)
        self._Entry1.pack(anchor='w',expand='yes',fill='both',side='top')
        self._Entry1.bind('<KeyRelease>',self._on_Entry1_KeyRel)
        self._Entry1.bind('<KeyRelease-Return>',self._on_Entry1_KeyRel_Return)
        self._Frame2 = Frame(self._Frame5)
        self._Frame2.pack(side='top')
        self._Label3 = Label(self._Frame2,font=('Times New Roman CE', 22, '')
            ,foreground='#ff0000',textvariable=self.EchoCont1)
        self._Label3.pack(expand='yes',fill='both',pady='1',side='top')
        self._Label4 = Label(self._Frame2,font=('Times New Roman CE', 22, '')
            ,foreground='#ff0000',textvariable=self.EchoCont2)
        self._Label4.pack(expand='yes',fill='both',side='bottom')
        self._Frame1 = Frame(self._Frame5)
        self._Frame1.pack(side='top')
        self._Entry2 = Entry(self._Frame1,textvariable=self.Echo2)
        self._Entry2.pack(anchor='w',expand='yes',fill='both',side='top')
        self._Entry2.bind('<KeyRelease>',self._on_Entry2_KeyRel)
        self._Entry2.bind('<KeyRelease-Return>',self._on_Entry2_KeyRel_Return)
        self._Label2 = Label(self._Frame1,anchor='s'
            ,font=('Times New Roman', 15, ''),text='Jobboldal')
        self._Label2.pack(anchor='s',expand='yes',fill='both',ipadx='100'
            ,side='top')
        #
        #Your code here
        #
        self.geometry("800x480+0+0")
        #self.attributes('-fullscreen', True)
        
        self._Entry1.focus_set()
        
    #
    #Start of event handler methods
    #


    def _on_Entry1_KeyRel(self,Event=None):
        self.EchoCont1.set(self.Echo.get())

    def _on_Entry1_KeyRel_Return(self,Event=None):
        baloldal.set(self.Echo.get())
        self.EchoCont1.set(self.Echo.get())
        #self.EchoCont1.set(self.ECHO1.get())
        self._Entry1.delete(0, END)
        self._Entry1.insert(0, "")
        Mark.set(1)
        self._Entry2.focus_set()
        if MarkRdy.get()==3:
            MarkRdy.set(0)
            self.destroy()
            
        
       

    def _on_Entry2_KeyRel(self,Event=None):
        self.EchoCont2.set(self.Echo2.get())

    def _on_Entry2_KeyRel_Return(self,Event=None):
        jobboldal.set(self.Echo2.get())
        self.EchoCont2.set(self.Echo2.get())
        
        self._Entry2.delete(0, END)
        self._Entry2.insert(0, "")
        Mark2.set(1)
        self._Entry1.focus_set()
        if MarkRdy.get()==3:
            MarkRdy.set(0)
            self.destroy()
            

    def _on__Button1_command(self,Event=None):
        ManMode().lift()
        ManMode().focus()
        self.destroy()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                   ManMode                                    #
#                                                                              #
#------------------------------------------------------------------------------#
class ManMode(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #
        import time
        self.Variable=IntVar()
        
   

        

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_ManMode_Key_Escape)
        self._Frame2 = Frame(self)
        self._Frame2.pack(expand='yes',fill='both',side='left')
        self._Button1 = Button(self._Frame2,bitmap='@balra.XBM'
            ,command=self._on__Button1_command)
        self._Button1.pack(anchor='e',side='right')
        self._Frame4 = Frame(self,background='#c0c0c0')
        self._Frame4.pack(expand='yes',fill='both',side='left')
        self._Label4 = Label(self._Frame4,background='#c0c0c0'
            ,font=('Times New Roman', 20, ''),foreground='#ff0000'
            ,relief='groove',takefocus=1,text='Work selection')
        self._Label4.pack(side='top')
        self._Label1 = Label(self._Frame4,background='#c0c0c0'
            ,font=('Times New Roman', 20, ''),foreground='#ff0000'
            ,relief='groove',text='Munkadarab kiválasztás')
        self._Label1.pack(side='top')
        self._Label2 = Label(self._Frame4,background='#c0c0c0',bitmap='@bal.XBM'
            ,relief='raised')
        self._Label2.pack(anchor='n',side='top')
        self._Label2.bind('<Any-ButtonPress>',self._on_Label2_Button__N)
        self._Label2.bind('<ButtonRelease-1>',self._on_Label2_ButRel_1)
        self._Label3 = Label(self._Frame4,background='#c0c0c0'
            ,bitmap='@jobb.XBM',relief='raised')
        self._Label3.pack(anchor='s',side='bottom')
        self._Label3.bind('<Any-ButtonPress>',self._on_Label3_Button__N)
        self._Label3.bind('<ButtonRelease-1>',self._on_Label3_ButRel_1)
        self._Frame3 = Frame(self)
        self._Frame3.pack(expand='yes',fill='both',side='left')
        self._Button2 = Button(self._Frame3,bitmap='@jobbra.XBM'
            ,command=self._on__Button2_command)
        self._Button2.pack(anchor='e',side='left')
        self._Button2.bind('<Any-ButtonRelease>',self._on_Button2_ButRel__N)
        #
        self.geometry("800x480+0+0")
        #self.attributes('-fullscreen', True)
        #Your code here
        if a==1:
            self.update()
            self._Label2.configure(relief='sunken')
            self._Label2.configure(background='#00ff00')
            
        else :
            self.update()
            self._Label2.configure(relief='raised')
            self._Label2.configure(background='#c0c0c0')
        
        if b==1:
            self.update()
            self._Label3.configure(relief='sunken')
            self._Label3.configure(background='#00ff00')
        elif b==0:
            self.update()
            self._Label3.configure(relief='raised')
            self._Label3.configure(background='#c0c0c0')
        else:
            pass
        
       
        
        

            
    #
    #Start of event handler methods
    #


    def _on_Button2_ButRel__N(self,Event=None):
        pass
        

    def _on_Frame1_Key_Escape(self,Event=None):
        escape()

    def _on_Frame2_Key_Escape(self,Event=None):
        escape()

    def _on_Frame3_Key_Escape(self,Event=None):
        escape()

    def _on_Label2_ButRel_1(self,Event=None):
        pass
       
        
      
        
        
        

    def _on_Label2_Button__N(self,Event=None):
        # Beragadó billentyű fölső
        FlipBal.get() 
        if a==1:
            self._Label2.configure(relief='sunken')
            self._Label2.configure(background='#00ff00')
            
        else:
            self._Label2.configure(relief='raised')
            self._Label2.configure(background='#c0c0c0')
        self.update()
       

    def _on_Label3_ButRel_1(self,Event=None):
        pass
        

    def _on_Label3_Button__N(self,Event=None):
        #Beragadó billentyű alsó
        FlipJobb.get()
        if b==1:
            self._Label3.configure(relief='sunken')
            self._Label3.configure(background='#00ff00')
        else:
            self._Label3.configure(relief='raised')
            self._Label3.configure(background='#c0c0c0')
        self.update()
        

    def _on__Button1_command(self,Event=None):
        Grav1().focus()
        self.after(200,self.destroy())

    def _on__Button2_command(self,Event=None):
        # Ki választja a megfelelő kézi beviteli képernyőt
        if a==1 and b==0:
            self.after(200,self.destroy())
            Man1().focus()
        elif a==0 and b==1:
            self.after(200,self.destroy())
            Man2().focus()
        elif a==1 and b==1:
            self.after(200,self.destroy())
            Man2x().focus()
        else:
            pass

    def on_ManMode_Key_Escape(self,Event=None):
        escape()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                   Manual1                                    #
#                                                                              #
#------------------------------------------------------------------------------#
class Manual1(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_Manual1_Key_Escape)
        self._Frame2 = Frame(self)
        self._Frame2.pack(side='top')
        self._Frame2.bind('<KeyPress-Escape>',self._on_Frame2_Key_Escape)
        self._Label2 = Label(self._Frame2,bitmap='@up.XBM')
        self._Label2.pack(side='left')
        self._Label4 = Label(self._Frame2,background='#c0c0c0',relief='raised'
            ,text='Table (UP / DOWN) \n Asztal (FÖL / LE)')
        self._Label4.pack(anchor='n',ipadx='20',pady='30',side='left')
        self._Label4.bind('<Any-ButtonPress>',self._on_Label2_Button__N)
        self._Label1 = Label(self._Frame2,bitmap='@down.XBM')
        self._Label1.pack(side='left')
        self._Frame1 = Frame(self)
        self._Frame1.pack(side='top')
        self._Frame1.bind('<KeyPress-Escape>',self._on_Frame1_Key_Escape)
        self._Label3 = Label(self._Frame1,background='#c0c0c0',relief='raised'
            ,text='Vacum select \n Vákum szivattyú')
        self._Label3.pack(ipadx='20',pady='30',side='left')
        self._Label3.bind('<Any-ButtonPress>',self._on_Label3_Button__N)
        self._Frame4 = Frame(self)
        self._Frame4.pack(side='top')
        self._Frame4.bind('<KeyPress-Escape>',self._on_Frame4_Key_Escape)
        self._Label5 = Label(self._Frame4,background='#c0c0c0',relief='raised'
            ,text='Camera select \n Kamera kiválaszt')
        self._Label5.pack(ipadx='20',pady='30',side='left')
        self._Label5.bind('<Any-ButtonPress>',self._on_Label5_Button__N)
        self._Frame5 = Frame(self)
        self._Frame5.pack(side='top')
        self._Label6 = Label(self._Frame5,background='#c0c0c0',relief='raised'
            ,text='Engrave select \n Gravírozó kiválaszt')
        self._Label6.pack(ipadx='20',pady='30',side='left')
        self._Label6.bind('<Any-ButtonPress>',self._on_Label6_Button__N)
        self._Frame3 = Frame(self)
        self._Frame3.pack(side='top')
        self._Frame3.bind('<KeyPress-Escape>',self._on_Frame3_Key_Escape)
        self._Button1 = Button(self._Frame3,command=self._on__Button1_command
            ,text='Change password \n Jelszó megváltoztatása')
        self._Button1.pack(ipadx='30',ipady='20',side='top')
        #
        
        self.geometry("800x480+0+0")
      
        #Your code here
        #
    #
    #Start of event handler methods
    #


    def _on_Frame1_Key_Escape(self,Event=None):
        escape()

    def _on_Frame2_Key_Escape(self,Event=None):
        escape()

    def _on_Frame3_Key_Escape(self,Event=None):
        escape()

    def _on_Frame4_Key_Escape(self,Event=None):
        escape()

    def _on_Label2_Button__N(self,Event=None):
         #Beragadó billentyű Pneumatika
        flipPneu.get()
        if PneuKiv==1:
            self._Label4.configure(relief='sunken')
            self._Label4.configure(background='#00ff00')
        else:
            self._Label4.configure(relief='raised')
            self._Label4.configure(background='#c0c0c0')
        self.update()
        

    def _on_Label3_Button__N(self,Event=None):
        #Beragadó billentyű Vákum
        flipVacum.get()
        if VacumKiv==1:
            self._Label3.configure(relief='sunken')
            self._Label3.configure(background='#00ff00')
        else:
            self._Label3.configure(relief='raised')
            self._Label3.configure(background='#c0c0c0')
        self.update()
        

    def _on_Label5_Button__N(self,Event=None):
        #Beragadó billentyű Kamera
        flipKamera.get()
        if KameraKiv==1:
            self._Label5.configure(relief='sunken')
            self._Label5.configure(background='#00ff00')
        else:
            self._Label5.configure(relief='raised')
            self._Label5.configure(background='#c0c0c0')
        self.update()
        

    def _on_Label6_Button__N(self,Event=None):
        #Beragadó billentyű 
        flipGravir.get()
        if GravirKiv==1:
            self._Label6.configure(relief='sunken')
            self._Label6.configure(background='#00ff00')
        else:
            self._Label6.configure(relief='raised')
            self._Label6.configure(background='#c0c0c0')
        self.update()
        

    def _on__Button1_command(self,Event=None):
        ChangePass().focus()
        self.destroy()

    def on_Manual1_Key_Escape(self,Event=None):
        #Beragadó billentyű alsó
        escape()
        self.destroy()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---
#------------------------------------------------------------------------------#
#                                                                              #
#                                ManualPassword                                #
#                                                                              #
#------------------------------------------------------------------------------#
class ManualPassword(Toplevel):
    def __init__(self,Master=None,*pos,**kw):
        #
        #Your code here
        #

        apply(Toplevel.__init__,(self,Master),kw)
        self.bind('<KeyPress-Escape>',self.on_ManualPassword_Key_Escape)
        self.Password = StringVar()
        self._Label1 = Label(self)
        self._Label1.pack(side='top')
        self._Entry1 = Entry(self,font=('Times New Roman', 28, '')
            ,justify='center',show='X',takefocus=1,textvariable=self.Password)
        self._Entry1.pack(side='top')
        self._Entry1.bind('<KeyPress-Return>',self._on_Entry1_Key_Return)
        self._Entry1.focus_set()
        #Your code here
        #
        #self.attributes('-fullscreen', True)
        self.geometry("800x480+0+0")
    #
    #Start of event handler methods
    #
        

    def _on_Entry1_Key_Return(self,Event=None):
        Pass=self.Password.get()
        self._Entry1.delete(0, END)
        self._Entry1.insert(0,'')
        if Pass==Password:
            Manual1().focus()
            self.after(200,self.destroy())
        else:
            InvalidPassword().focus()
            
            
           

    def on_ManualPassword_Key_Escape(self,Event=None):
        escape()
    #
    #Start of non-Rapyd user code
    #


pass #---end-of-form---

try:
    #--------------------------------------------------------------------------#
    # User code should go after this comment so it is inside the "try".        #
    #     This allows rpErrorHandler to gain control on an error so it         #
    #     can properly display a Rapyd-aware error message.                    #
    #--------------------------------------------------------------------------#
    
    Password='iddqd'
    Pass='0'
    keyboard='NULL'
    a=0
    b=0
    PneuKiv=0
    VacumKiv=0
    KameraKiv=0
    GravirKiv=0
 


    def flipPneum(*args):
        global PneuKiv
        PneuKiv=int(not PneuKiv) 
        
    def flipVacumm(*args):
        global VacumKiv
        VacumKiv=int(not VacumKiv)
        
    def flipKameram(*args):
        global KameraKiv
        KameraKiv=int(not KameraKiv)
        
    def flipGravirm(*args):
        global GravirKiv
        GravirKiv=int(not GravirKiv)
    

    def escape():
            Grav1().focus()

    def flipperA(*args):
            global a
            a=int(not a)
            
    def flipperB(*args):
        global b
        b=int(not b)
            
    def trigger(*args):
        if var==1:
            ManMode()._on_Label1_ButRel_1()
        else:
            ManMode()._on_Button2_Deact()
            
    def isready2(*args):
        if Mark.get()+Mark2.get()==2:
            MarkRdy.set(3)
        else:
            pass
       
            
    def markrdy2x(*args):
        if MarkRdy.get()==3:
            Mark.set(0)
            Mark2.set(0)
            Confirm2x().focus()
        
            
            
    

    #Adjust sys.path so we can find other modules of this project
    import sys
    if '.' not in sys.path:
        sys.path.append('.')
    #Put lines to import other modules of this project here
     
    
    if __name__ == '__main__':

        Root = Tk()
        import Tkinter
        Tkinter.CallWrapper = rpErrorHandler.CallWrapper
        del Tkinter
        App = Intro1(Root)
        App.pack(expand='yes',fill='both')

        Root.geometry("800x480+0+0")
        #Root.attributes('-fullscreen', True)
        Root.title('Intro1')  
        Root.bind('<Escape>',quit)
        #Vátozók outputra
        baloldal=StringVar()
        jobboldal=StringVar()
        #Változók inputra
        BalInp=StringVar()
        JobbInp=StringVar()
        #Belső változók
        Mark=IntVar()
        Mark2=IntVar()
        Mark.trace('w',isready2)
        Mark2.trace('w',isready2)
        MarkRdy=IntVar()
        MarkRdy.trace('w',markrdy2x)
        
        
        #Beragadó billentyühöz
        
        #Pneumatika Manual mode
        flipPneu=IntVar()
        flipPneu.trace('r',flipPneum)
        
        #Vákum szivattyú Manual mode
        flipVacum=IntVar()
        flipVacum.trace('r',flipVacumm)
        
        #Kamera Manual mode
        flipKamera=IntVar()
        flipKamera.trace('r',flipKameram)
        
        #Gravírozó Manual mode
        flipGravir=IntVar()
        flipGravir.trace('r',flipGravirm)
        
        #Bal és jobb kiválaszt
        var=IntVar()
        FlipBal=StringVar()
        FlipJobb=StringVar()
        FlipBal.trace('r',flipperA)
        FlipJobb.trace('r',flipperB)
        var.trace('w',trigger)
      
  
        # controll
        print Root.focus_get()
        
        def quit(Event=None):
            sys.exit()
        Root.mainloop()
    #--------------------------------------------------------------------------#
    # User code should go above this comment.                                  #
    #--------------------------------------------------------------------------#
except:
    rpErrorHandler.RunError()