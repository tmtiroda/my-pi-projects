import string
import serial

class rswrite:
        
    def write(self):
        writeBuffer = 'b'
        wt = writeBuffer


        print('this sample application writes to the rs-232 port')

        # open the port
        print('opening device /dev/ttyRPC0')
        try:
            ser = serial.Serial(port='/dev/ttyRPC0', baudrate=9600, writeTimeout=1)
        except:
            print('failed.')
            print('possible causes:')
            print('1) the raspicomm device driver is not loaded. type \'lsmod\' and verify that you \'raspicommrs485\' is loaded.')
            print('2) the raspicomm device driver is in use. Is another application using the device driver?')
            print('3) something went wrong when loading the device driver. type \'dmesg\' and check the kernel messages')
            exit()

        print('successful.')
        # write
        print('start writing to the rs-232 port', wt)
        ser.write(wt)
        a=ser.write(wt)
        print(a)
        ser.close
        return wt
        
writ=rswrite()
writ.write()