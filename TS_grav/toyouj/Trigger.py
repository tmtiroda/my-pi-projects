#!/usr/bin/env python

import OPP
import time
import smbus

bus = smbus.SMBus(1) # Rev 2 Pi uses 1
Trigger=OPP.OPUT()

def Trigg():
    
    Trigger.OPa4=1
    Trigger.Write()
    time.sleep(1)
    Trigger.OPb1=1
    Trigger.Write()
    time.sleep(0.2)
    Trigger.OPb1=0
    Trigger.Write()
    time.sleep(1)
    Trigger.Default()
    
def MarkSTOP():
    Trigger.OPb4=1
    Trigger.Write()
    
MarkSTOP()
Trigg()
